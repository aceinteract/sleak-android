package com.aceinteract.android.sleak.data.remote.source

import android.content.Context
import com.aceinteract.android.sleak.data.model.Album
import com.aceinteract.android.sleak.data.remote.api.ApiCallback
import com.aceinteract.android.sleak.data.remote.api.ApiClient
import com.aceinteract.android.sleak.data.source.AlbumsDataSource
import com.aceinteract.android.sleak.util.AppExecutors
import retrofit2.Call
import retrofit2.Response

class AlbumsRemoteDataSource(val context: Context, val appExecutors: AppExecutors) : AlbumsDataSource {

    private val apiClient = ApiClient()

    override fun getAlbum(albumId: String, callback: AlbumsDataSource.GetAlbumCallback) {
        appExecutors.networkIO.execute {

            apiClient.getAlbum(albumId, object : ApiCallback<Album> {
                override fun onFail(call: Call<Album>?, t: Throwable?) {
                    appExecutors.mainThread.execute { callback.onDataNotAvailable() }
                }

                override fun onResponse(call: Call<Album>?, response: Response<Album>?) {
                    appExecutors.mainThread.execute {

                        when (response!!.code()) {

                            200 -> callback.onAlbumLoaded(response.body()!!)
                            else -> callback.onDataNotAvailable()

                        }

                    }
                }

            })

        }
    }

}