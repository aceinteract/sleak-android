package com.aceinteract.android.sleak.ui.auth.login

import android.app.Application
import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.databinding.ObservableBoolean
import android.databinding.ObservableField
import android.support.annotation.VisibleForTesting
import com.aceinteract.android.sleak.Injection
import com.aceinteract.android.sleak.R
import com.aceinteract.android.sleak.SingleLiveEvent
import com.aceinteract.android.sleak.SleakApplication
import com.aceinteract.android.sleak.data.model.Account
import com.aceinteract.android.sleak.data.repository.AccountsRepository
import com.aceinteract.android.sleak.data.source.AccountsDataSource
import com.aceinteract.android.sleak.ui.base.BaseViewModel

class LoginViewModel(application: Application, private val accountsRepository: AccountsRepository) : BaseViewModel(application) {

    val dataLoading = ObservableBoolean(false)

    val username = ObservableField<String>()
    val password = ObservableField<String>()

    internal val loggedInEvent = SingleLiveEvent<Account>()

    fun attemptLogin() {
        val usernameValue = username.get()
        val passwordValue = password.get()
        if (usernameValue != null && passwordValue != null) {
            dataLoading.set(true)
            accountsRepository.loginAccount(usernameValue, passwordValue, object : AccountsDataSource.LoginCallback {
                override fun onLoginSuccess(account: Account) {
                    dataLoading.set(false)
                    loggedInEvent.value = account
                }

                override fun onLoginFailure(reason: Int) {
                    dataLoading.set(false)
                    when (reason) {
                        Account.LoginFailureReason.USER_NOT_FOUND -> showSnackbarMessage(R.string.invalid_username)
                        Account.LoginFailureReason.INVALID_PASSWORD -> showSnackbarMessage(R.string.invalid_password)
                        Account.LoginFailureReason.USER_BLOCKED -> showSnackbarMessage(R.string.blocked_user)
                        Account.LoginFailureReason.SERVER_ERROR -> showSnackbarMessage(R.string.server_error)
                    }
                }

            })
        } else {
            showSnackbarMessage(R.string.empty_fields)
        }
    }

}

@Suppress("UNCHECKED_CAST")
class LoginViewModelFactory
private constructor(private val application: Application,
                    private val accountsRepository: AccountsRepository) : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T =
            with(modelClass) {
                if (isAssignableFrom(LoginViewModel::class.java)) LoginViewModel(application, accountsRepository)
                else throw IllegalArgumentException("Unknown ViewModel class: ${modelClass.name}")
            } as T

    companion object {

        @Volatile
        var INSTANCE: LoginViewModelFactory? = null

        fun getInstance(application: SleakApplication) =
                INSTANCE ?: synchronized(LoginViewModel::class.java) {
                    INSTANCE ?: LoginViewModelFactory(application,
                            Injection.provideAccountsRepository(application.applicationContext))
                            .also { INSTANCE = it }
                }

        @VisibleForTesting
        fun destroyInstance() {
            INSTANCE = null
        }

    }
}