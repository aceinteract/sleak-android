package com.aceinteract.android.sleak.ui.main.album

import android.databinding.BindingAdapter
import android.support.v7.widget.RecyclerView
import android.util.Log
import com.aceinteract.android.sleak.data.model.Song

object SongsRecyclerViewBindings {

    @BindingAdapter("app:items")
    @JvmStatic
    fun RecyclerView.setItems(items: ArrayList<Song>?) {
        if (items != null) {
            Log.i("TAG", items.toString())
            items.sortedBy {
                it.trackNumber
            }
            with(adapter as SongsAdapter) {
                replaceData(items)
            }
        }
    }

}