package com.aceinteract.android.sleak.data.source

import com.aceinteract.android.sleak.data.entity.Song

interface SongsDataSource {

    interface LoadSongsCallback : DataSourceCallback {

        fun onSongsLoaded(songs: ArrayList<Song>)

    }

    interface GetSongCallback : DataSourceCallback {

        fun onSongLoaded(song: Song)

    }

    fun getSongs(callback: LoadSongsCallback)

    fun getSong(songId: String, callback: GetSongCallback)

    fun saveSong(song: Song)

    fun refreshSongs()

    fun deleteAllSongs()

    fun deleteSong(songId: String)

}