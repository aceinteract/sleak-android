package com.aceinteract.android.sleak.data.source

interface LibraryDataSource {

    fun addSongsToLibrary(libraryId: String, songIds: ArrayList<String>)

    fun addAlbumsToLibrary(libraryId: String, albumIds: ArrayList<String>)

}