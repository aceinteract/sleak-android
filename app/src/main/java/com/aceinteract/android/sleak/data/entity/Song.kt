package com.aceinteract.android.sleak.data.entity

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import java.util.UUID

@Entity(tableName = "songs")
data class Song constructor(
        @PrimaryKey var id: String = UUID.randomUUID().toString(),
        var title: String,
        var album: String,
        var artist: String
)