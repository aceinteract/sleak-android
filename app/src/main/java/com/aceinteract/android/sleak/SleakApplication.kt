package com.aceinteract.android.sleak

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.IBinder
import android.support.multidex.MultiDexApplication
import com.aceinteract.android.sleak.data.model.Song
import com.aceinteract.android.sleak.service.MusicService
import com.aceinteract.android.sleak.util.StorageUtil
import uk.co.chrisjenx.calligraphy.CalligraphyConfig

class SleakApplication : MultiDexApplication() {

    private lateinit var musicService: MusicService

    private lateinit var serviceIntent: Intent

    private var isBound = false

    private var songIndex = 0

    private var queue = ArrayList<Song>()

    private lateinit var storageUtil: StorageUtil

    var isSongPlaying = false
        get() = if (isBound) musicService.isPlaying else false

    var songDuration = 0
        get() = if (isBound) musicService.duration else 0

    private val musicServiceConnection = object : ServiceConnection {
        override fun onServiceDisconnected(name: ComponentName) {
            isBound = false
        }

        override fun onServiceConnected(name: ComponentName, service: IBinder) {
            val binder = service as MusicService.MusicBinder
            musicService = binder.service
            isBound = true

            musicService.songQueue = queue
            musicService.currentSong = songIndex
            musicService.playSong()
        }
    }

    override fun onCreate() {
        super.onCreate()

        INSTANCE = this

        storageUtil = StorageUtil(this)
        queue = storageUtil.queue
        songIndex = storageUtil.currentQueuePosition

        CalligraphyConfig.initDefault(CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Montserrat-Regular.ttf")
                .setFontAttrId(uk.co.chrisjenx.calligraphy.R.attr.fontPath).build())
    }

    override fun onTerminate() {
        super.onTerminate()
        stopService(serviceIntent)
        if (isBound) unbindService(musicServiceConnection); musicService.stopSelf()
    }

    fun playSongOnQueue(position: Int) {
        songIndex = position
        if (isBound) {
            musicService.currentSong = position
            musicService.playSong()
        } else {
            serviceIntent = Intent(this, MusicService::class.java)
            bindService(serviceIntent, musicServiceConnection, Context.BIND_AUTO_CREATE)
            startService(serviceIntent)
        }
    }

    fun startPlaying() {
        songIndex = storageUtil.currentQueuePosition
        if (isBound) musicService.startPlayer()
        else {
            serviceIntent = Intent(this, MusicService::class.java)
            bindService(serviceIntent, musicServiceConnection, Context.BIND_AUTO_CREATE)
            startService(serviceIntent)
        }
    }

    fun pauseSong() = musicService.pauseSong()

    fun seekSongTo(position: Int) = musicService.seek(position)

    fun playPreviousSong() {
        if (isBound) musicService.playPrev()
        else {
            songIndex--
            if (songIndex < 0) songIndex = 0
            serviceIntent = Intent(this, MusicService::class.java)
            bindService(serviceIntent, musicServiceConnection, Context.BIND_AUTO_CREATE)
            startService(serviceIntent)
        }
        songIndex = storageUtil.currentQueuePosition
    }

    fun playNextSong() {
        if (isBound) musicService.playNext()
        else {
            songIndex++
            if (songIndex > queue.size - 1) songIndex = 0
            serviceIntent = Intent(this, MusicService::class.java)
            bindService(serviceIntent, musicServiceConnection, Context.BIND_AUTO_CREATE)
            startService(serviceIntent)
        }
        songIndex = storageUtil.currentQueuePosition
    }

    companion object {

        @get:Synchronized
        var INSTANCE: SleakApplication? = null
            private set

        const val MUSIC_NOTIFICATION_ID = 9999

        const val MAIN_BROADCAST = "com.aceinteract.sleak.MAIN_BROADCAST"

        const val BROADCAST_IS_PLAYING = "is_playing"
        const val BROADCAST_TITLE = "title"
        const val BROADCAST_ARTISTS = "artists"
        const val BROADCAST_ARTIST_IDS = "artist_ids"
        const val BROADCAST_SONG_ID = "song_id"
        const val BROADCAST_ALBUM_TITLE = "album_title"
        const val BROADCAST_ALBUM_ID = "album_id"

        // const val baseUrl = "http://192.168.43.189:8080/"
        const val baseUrl = "http://192.168.240.2:8080/"

    }

}