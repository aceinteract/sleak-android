package com.aceinteract.android.sleak.data.model

import java.util.*
import kotlin.annotation.AnnotationTarget.*

data class Account(
        val id: String,
        val email: String,
        val username: String,
        val rawPassword: String,
        val followerCount: Int = 0,
        val followingCount: Int = 0,
        val fullName: String = "",
        val bio: String = "",
        var library: Library? = null,
        val gcmId: String? = null,
        val twitterId: String? = null,
        val facebookId: String? = null,
        val gender: Int = GENDER_NA,
        val dateOfBirth: Date? = null,
        val albums: List<Album> = ArrayList(),
        val playlists: List<Playlist> = ArrayList(),
        val isArtist: Boolean = false,
        val isActive: Boolean = true
) {

    override fun toString(): String {
        return fullName
    }

    companion object {

        const val GENDER_NA = 0
        const val GENDER_MALE = 1
        const val GENDER_FEMALE = 2

    }

    class LoginFailureReason {

        @Retention(AnnotationRetention.BINARY)
        @Target(VALUE_PARAMETER, FUNCTION, PROPERTY_GETTER, PROPERTY_SETTER, LOCAL_VARIABLE, FIELD)
        annotation class LoginFailureReasonInt

        companion object {
            @LoginFailureReasonInt
            const val USER_NOT_FOUND = 0

            @LoginFailureReasonInt
            const val INVALID_PASSWORD = 1

            @LoginFailureReasonInt
            const val USER_BLOCKED = 2

            @LoginFailureReasonInt
            const val SERVER_ERROR = 3
        }
    }

}