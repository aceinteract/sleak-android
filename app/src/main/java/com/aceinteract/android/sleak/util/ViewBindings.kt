package com.aceinteract.android.sleak.util

import android.app.Activity
import android.content.ContextWrapper
import android.databinding.BindingAdapter
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.graphics.drawable.GradientDrawable
import android.os.Build
import android.support.constraint.ConstraintLayout
import android.support.design.widget.CollapsingToolbarLayout
import android.support.design.widget.FloatingActionButton
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.graphics.Palette
import android.util.Log
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.ImageView
import android.widget.TextView
import com.aceinteract.android.sleak.R
import com.aceinteract.android.sleak.SleakApplication
import com.aceinteract.android.sleak.ui.main.home.HomeViewModel
import com.aceinteract.android.sleak.ui.songs.SongsViewModel
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.BitmapImageViewTarget
import com.bumptech.glide.request.transition.Transition

@BindingAdapter("app:drawableSrc")
fun FloatingActionButton.setDrawableImage(drawable: Drawable) = setImageDrawable(drawable)

@BindingAdapter("app:imageUrl")
fun ImageView.loadImageFromUrl(imageUrl: String) {

    Glide.with(context).asBitmap()
            .load("${SleakApplication.baseUrl}api/v1/$imageUrl")
            .thumbnail(.4f)
            .apply(RequestOptions().placeholder(android.R.color.darker_gray))
            .into(object : BitmapImageViewTarget(this) {
                override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                    super.onResourceReady(resource, transition)
                    val mutedColor = Palette.from(resource).generate()
                            .getDarkMutedColor(context.resources.getColor(android.R.color.black))
                    val vibrantColor = Palette.from(resource).generate()
                            .getDarkVibrantColor(mutedColor)

                    val gradientOverlay = IntArray(2)
                    gradientOverlay[0] = Color.TRANSPARENT
                    gradientOverlay[1] = vibrantColor

                    (parent as View).findViewById<ConstraintLayout>(R.id.cl_artist_info)?.background =
                            GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, gradientOverlay)
//                    (parent as View).findViewById<FloatingActionButton>(R.id.fab_song_play)?.backgroundTintList =
//                            ColorDrawable(vibrantColor)

                    val collapsingToolbar = (parent as View).findViewById<CollapsingToolbarLayout>(R.id.collapsing_toolbar)
                    if (collapsingToolbar != null) {
                        collapsingToolbar.contentScrim = ColorDrawable(vibrantColor)
                        collapsingToolbar.statusBarScrim = ColorDrawable(vibrantColor)
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            Log.i("TAG", context::class.qualifiedName)
                            val window: Window = if (context is Activity) (context as Activity).window
                            else ((context as ContextWrapper).baseContext as Activity).window
                            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                            window.statusBarColor = vibrantColor.adjustAlpha(230)
                        }
                    }
                }
            })

}

@BindingAdapter("android:onRefresh")
fun SwipeRefreshLayout.setSwipeRefreshLayoutOnRefreshListener(
        viewModel: SongsViewModel) {
    setOnRefreshListener { viewModel.loadSongs(true, true) }
}

@BindingAdapter("android:onRefresh")
fun SwipeRefreshLayout.setSwipeRefreshLayoutOnRefreshListener(
        viewModel: HomeViewModel) {
    setOnRefreshListener { viewModel.loadRecommendations(true) }
}