package com.aceinteract.android.sleak.ui.auth

import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.aceinteract.android.sleak.R
import com.aceinteract.android.sleak.ui.auth.login.LoginFragment
import com.aceinteract.android.sleak.ui.auth.signup.SignUpFragment
import com.aceinteract.android.sleak.ui.base.BaseActivity
import kotlinx.android.synthetic.main.activity_auth.*

class AuthActivity : BaseActivity() {

    private var pagerAdapter: AuthPagerAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_auth)

        pagerAdapter = AuthPagerAdapter(supportFragmentManager)

        container.adapter = pagerAdapter

        container.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabs))
        tabs.addOnTabSelectedListener(TabLayout.ViewPagerOnTabSelectedListener(container))

    }

    inner class AuthPagerAdapter(fragmentManager: FragmentManager) : FragmentPagerAdapter(fragmentManager) {

        override fun getItem(position: Int): Fragment = when (position) {
            0 -> LoginFragment()
            1 -> SignUpFragment()
            else -> LoginFragment()
        }

        override fun getCount(): Int {
            return 2
        }
    }

}
