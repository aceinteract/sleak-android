package com.aceinteract.android.sleak.ui.main.home

import android.databinding.BindingAdapter
import android.support.v7.widget.RecyclerView
import com.aceinteract.android.sleak.data.model.Recommendation

object RecommendationsRecyclerViewBindings {

    @BindingAdapter("app:items")
    @JvmStatic
    fun RecyclerView.setItems(items: ArrayList<Recommendation>) {
        with(adapter as RecommendationsAdapter) {
            replaceData(items)
        }
    }

}