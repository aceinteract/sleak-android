package com.aceinteract.android.sleak

import android.content.Context
import com.aceinteract.android.sleak.data.local.SleakDatabase
import com.aceinteract.android.sleak.data.local.source.SongsLocalDataSource
import com.aceinteract.android.sleak.data.remote.source.AccountsRemoteDataSource
import com.aceinteract.android.sleak.data.remote.source.AlbumsRemoteDataSource
import com.aceinteract.android.sleak.data.remote.source.RecommendationsRemoteDataSource
import com.aceinteract.android.sleak.data.remote.source.SongsRemoteDataSource
import com.aceinteract.android.sleak.data.repository.AccountsRepository
import com.aceinteract.android.sleak.data.repository.AlbumsRepository
import com.aceinteract.android.sleak.data.repository.RecommendationsRepository
import com.aceinteract.android.sleak.data.repository.SongsRepository
import com.aceinteract.android.sleak.util.AppExecutors
import com.aceinteract.android.sleak.util.StorageUtil

object Injection {

    fun provideSongsRepository(context: Context): SongsRepository {
        val database = SleakDatabase.getInstance(context)
        return SongsRepository.getInstance(SongsRemoteDataSource(context, AppExecutors()),
                SongsLocalDataSource.getInstance(AppExecutors(), database.songsDataAccessObject()))
    }

    fun provideAccountsRepository(context: Context): AccountsRepository {
        return AccountsRepository.getInstance(StorageUtil(context), AccountsRemoteDataSource(AppExecutors()))
    }

    fun provideRecommendationsRepository(context: Context): RecommendationsRepository {
        return RecommendationsRepository.getInstance(RecommendationsRemoteDataSource(context, AppExecutors()))
    }

    fun provideAlbumsRepository(context: Context): AlbumsRepository {
        return AlbumsRepository.getInstance(AlbumsRemoteDataSource(context, AppExecutors()))
    }
}
