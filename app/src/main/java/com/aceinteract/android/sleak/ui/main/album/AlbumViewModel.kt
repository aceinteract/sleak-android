package com.aceinteract.android.sleak.ui.main.album

import android.annotation.SuppressLint
import android.app.Application
import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.databinding.ObservableBoolean
import android.databinding.ObservableField
import android.support.annotation.VisibleForTesting
import android.util.Log
import com.aceinteract.android.sleak.Injection
import com.aceinteract.android.sleak.R
import com.aceinteract.android.sleak.SingleLiveEvent
import com.aceinteract.android.sleak.SleakApplication
import com.aceinteract.android.sleak.data.model.Album
import com.aceinteract.android.sleak.data.model.Song
import com.aceinteract.android.sleak.data.remote.source.LibraryRemoteDataSource
import com.aceinteract.android.sleak.data.source.AlbumsDataSource
import com.aceinteract.android.sleak.data.repository.AlbumsRepository
import com.aceinteract.android.sleak.ui.base.BaseViewModel
import com.aceinteract.android.sleak.ui.main.home.HomeViewModelFactory
import com.aceinteract.android.sleak.util.AppExecutors

class AlbumViewModel(context: Application, private val albumsRepository: AlbumsRepository,
                     private val libraryDataSource: LibraryRemoteDataSource) : BaseViewModel(context) {

    private val isLoadingError = ObservableBoolean(false)

    @SuppressLint("StaticFieldLeak")
    private val context = context.applicationContext

    internal var playSongEvent = SingleLiveEvent<Int>()
    internal var addToLibraryEvent = SingleLiveEvent<String>()
    internal var addSongToLibraryEvent = SingleLiveEvent<Song>()
    internal var openArtistEvent = SingleLiveEvent<String>()

    val isInLibrary = ObservableBoolean(false)

    val album: ObservableField<Album> = ObservableField()
    val albumLoading = ObservableBoolean(false)

    fun start(albumId: String) {
        loadAlbum(albumId)
    }

    fun loadAlbum(albumId: String, showLoadingUI: Boolean = false) {

        if (showLoadingUI) albumLoading.set(true)

        albumsRepository.getAlbum(albumId, object : AlbumsDataSource.GetAlbumCallback {

            override fun onAlbumLoaded(album: Album) {
                if (showLoadingUI) albumLoading.set(false)
                isLoadingError.set(false)
                isInLibrary.set(!storageUtil.library!!.albums.none { it.id == album.id })
                Log.i("TAG", album.id)
                album.songs.forEach {
                    it.album = Album(albumId, album.title, album.dateReleased, album.artists)
                }
                with(this@AlbumViewModel.album) {
                    set(album)
                }
            }

            override fun onDataNotAvailable() {
                if (showLoadingUI) albumLoading.set(false)
                isLoadingError.set(true)
                showSnackbarMessage(R.string.loading_data_error)
            }

        })

    }

    fun playSong(index: Int) {
        storageUtil.queue = album.get()!!.songs
        SleakApplication.INSTANCE!!.playSongOnQueue(index)
    }

    fun addAlbumToLibrary() {
        val albumIds = ArrayList<String>().also {
            album.get()?.let { (id) -> it.add(id) }
        }
        isInLibrary.set(true)
        storageUtil.library = storageUtil.library?.apply {
            album.get()?.let { albums.add(it) }
        }
        libraryDataSource.addAlbumsToLibrary(storageUtil.library!!.id, albumIds)
    }

    fun addSongToLibrary(song: Song) {
        val songIds = ArrayList<String>().also {
            it.add(song.id)
        }
        storageUtil.library = storageUtil.library?.apply {
            songs.add(song)
        }
        libraryDataSource.addSongsToLibrary(storageUtil.library!!.id, songIds)
    }

}

@Suppress("UNCHECKED_CAST")
class AlbumViewModelFactory private constructor(private val context: Application,
                                                private val albumsRepository: AlbumsRepository,
                                                private val libraryDataSource: LibraryRemoteDataSource)
    : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T =
            with(modelClass) {
                if (isAssignableFrom(AlbumViewModel::class.java)) AlbumViewModel(context, albumsRepository, libraryDataSource)
                else throw IllegalArgumentException("Unknown ViewModel class: ${modelClass.name}")
            } as T

    companion object {

        @Volatile
        var INSTANCE: AlbumViewModelFactory? = null

        fun getInstance(application: SleakApplication) =
                INSTANCE ?: synchronized(AlbumViewModelFactory::class.java) {
                    INSTANCE ?: AlbumViewModelFactory(
                            application,
                            Injection.provideAlbumsRepository(application.applicationContext),
                            LibraryRemoteDataSource(AppExecutors())
                    ).also { INSTANCE = it }
                }

        @VisibleForTesting
        fun destroyInstance() {
            HomeViewModelFactory.INSTANCE = null
        }

    }

}