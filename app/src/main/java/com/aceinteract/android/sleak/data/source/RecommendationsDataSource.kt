package com.aceinteract.android.sleak.data.source

import com.aceinteract.android.sleak.data.model.Recommendation

interface RecommendationsDataSource {

    interface LoadRecommendationsCallback : DataSourceCallback {

        fun onRecommendationsLoaded(recommendations: ArrayList<Recommendation>)

    }

    fun getRecommendations(callback: LoadRecommendationsCallback)

}