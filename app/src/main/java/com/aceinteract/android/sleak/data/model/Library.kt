package com.aceinteract.android.sleak.data.model

import java.util.*

data class Library(
        val user: Account,
        var songs: MutableList<Song> = ArrayList(),
        var albums: MutableList<Album> = ArrayList(),
        val id: String = UUID.randomUUID().toString()
)