package com.aceinteract.android.sleak.ui.main.home

import android.annotation.SuppressLint
import android.app.Application
import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.databinding.ObservableArrayList
import android.databinding.ObservableBoolean
import android.support.annotation.VisibleForTesting
import com.aceinteract.android.sleak.Injection
import com.aceinteract.android.sleak.R
import com.aceinteract.android.sleak.SingleLiveEvent
import com.aceinteract.android.sleak.SleakApplication
import com.aceinteract.android.sleak.data.model.Recommendation
import com.aceinteract.android.sleak.data.repository.RecommendationsRepository
import com.aceinteract.android.sleak.data.source.RecommendationsDataSource
import com.aceinteract.android.sleak.ui.base.BaseViewModel

class HomeViewModel(application: Application, private val recommendationsRepository: RecommendationsRepository)
    : BaseViewModel(application) {

    private val isRecommendationsLoadingError = ObservableBoolean(false)

    @SuppressLint("StaticFieldLeak")
    private val context = application.applicationContext

    internal val openAlbumEvent = SingleLiveEvent<String>()
    internal val openPlaylistEvent = SingleLiveEvent<String>()

    val recommendations: ObservableArrayList<Recommendation> = ObservableArrayList()
    val recommendationsLoading = ObservableBoolean(false)
    val recommendationsEmpty = ObservableBoolean(false)

    fun start() {
        loadRecommendations()
    }

    fun loadRecommendations(showLoadingUI: Boolean = false) {
        if (showLoadingUI) {
            recommendationsLoading.set(true)
        }
        recommendationsRepository.getRecommendations(object : RecommendationsDataSource.LoadRecommendationsCallback {
            override fun onRecommendationsLoaded(recommendations: ArrayList<Recommendation>) {
                if (showLoadingUI) recommendationsLoading.set(false)
                isRecommendationsLoadingError.set(false)
                with(this@HomeViewModel.recommendations) {
                    clear()
                    addAll(recommendations)
                    recommendationsEmpty.set(isEmpty())
                }
            }

            override fun onDataNotAvailable() {
                if (showLoadingUI) recommendationsLoading.set(false)
                isRecommendationsLoadingError.set(true)
                showSnackbarMessage(R.string.loading_data_error)
            }
        })
    }

}

@Suppress("UNCHECKED_CAST")
class HomeViewModelFactory private constructor(private val context: Application, private val recommendationsRepository: RecommendationsRepository) : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T =
            with(modelClass) {
                if (isAssignableFrom(HomeViewModel::class.java)) HomeViewModel(context, recommendationsRepository)
                else throw IllegalArgumentException("Unknown ViewModel class: ${modelClass.name}")
            } as T

    companion object {

        @Volatile
        var INSTANCE: HomeViewModelFactory? = null

        fun getInstance(application: SleakApplication) =
                INSTANCE ?: synchronized(HomeViewModelFactory::class.java) {
                    INSTANCE ?: HomeViewModelFactory(application,
                            Injection.provideRecommendationsRepository(application.applicationContext))
                            .also { INSTANCE = it }
                }

        @VisibleForTesting
        fun destroyInstance() {
            INSTANCE = null
        }

    }
}