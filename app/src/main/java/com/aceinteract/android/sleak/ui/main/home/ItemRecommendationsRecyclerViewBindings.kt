package com.aceinteract.android.sleak.ui.main.home

import android.databinding.BindingAdapter
import android.support.v7.widget.RecyclerView
import com.aceinteract.android.sleak.data.model.Album
import com.aceinteract.android.sleak.data.model.Playlist

object AlbumRecommendationsRecyclerViewBindings {

    @BindingAdapter("app:items")
    @JvmStatic
    fun RecyclerView.setItems(items: ArrayList<Album>) {
        if (items.isNotEmpty()) {

            with(adapter as AlbumRecommendationsAdapter) {
                replaceData(items)
            }
        }
    }

}

object PlaylistRecommendationsRecyclerViewBindings {

    @BindingAdapter("app:items")
    @JvmStatic
    fun RecyclerView.setItems(items: ArrayList<Playlist>) {
        if (items.isNotEmpty()) {

            with(adapter as PlaylistRecommendationsAdapter) {
                replaceData(items)
            }
        }
    }

}