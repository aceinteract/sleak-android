package com.aceinteract.android.sleak.data.repository

import com.aceinteract.android.sleak.data.entity.Song
import com.aceinteract.android.sleak.data.source.SongsDataSource

class SongsRepository(val songsRemoteDataSource: SongsDataSource, val songsLocalDataSource: SongsDataSource) : BaseRepository(), SongsDataSource {

    var cachedSongs: LinkedHashMap<String, Song> = LinkedHashMap()
    var cacheIsDirty = false

    override fun getSongs(callback: SongsDataSource.LoadSongsCallback) {
        if (cachedSongs.isNotEmpty() && !cacheIsDirty) {
            callback.onSongsLoaded(ArrayList(cachedSongs.values))
            return
        }

        if (cacheIsDirty) {
            getSongsFromRemoteDataSource(callback)
        } else {
            songsLocalDataSource.getSongs(object : SongsDataSource.LoadSongsCallback {
                override fun onSongsLoaded(songs: ArrayList<Song>) {
                    refreshCache(songs)
                    if (songs.isEmpty()) getSongsFromRemoteDataSource(callback)
                    else callback.onSongsLoaded(ArrayList(cachedSongs.values))
                }

                override fun onDataNotAvailable() {
                    getSongsFromRemoteDataSource(callback)
                }

            })
        }
    }

    override fun getSong(songId: String, callback: SongsDataSource.GetSongCallback) {
        val songInCache = getSongById(songId)

        if (songInCache != null) {
            callback.onSongLoaded(songInCache)
            return
        }

        songsLocalDataSource.getSong(songId, object : SongsDataSource.GetSongCallback {

            override fun onSongLoaded(song: Song) {
                cacheSong(song) {
                    callback.onSongLoaded(it)
                }
            }

            override fun onDataNotAvailable() {
                songsRemoteDataSource.getSong(songId, object : SongsDataSource.GetSongCallback {
                    override fun onSongLoaded(song: Song) {
                        cacheSong(song) {
                            callback.onSongLoaded(it)
                        }
                    }

                    override fun onDataNotAvailable() {
                        callback.onDataNotAvailable()
                    }
                })
            }

        })
    }

    override fun refreshSongs() {
        cacheIsDirty = true
    }

    override fun saveSong(song: Song) {
        cacheSong(song) {
            songsLocalDataSource.saveSong(song)
            songsRemoteDataSource.saveSong(song)
        }
    }

    override fun deleteAllSongs() {
        songsRemoteDataSource.deleteAllSongs()
        songsLocalDataSource.deleteAllSongs()
        cachedSongs.clear()
    }

    override fun deleteSong(songId: String) {
        songsLocalDataSource.deleteSong(songId)
        songsRemoteDataSource.deleteSong(songId)
        cachedSongs.remove(songId)
    }

    private fun getSongById(id: String) = cachedSongs[id]

    private fun getSongsFromRemoteDataSource(callback: SongsDataSource.LoadSongsCallback) {
        songsRemoteDataSource.getSongs(object : SongsDataSource.LoadSongsCallback {
            override fun onSongsLoaded(songs: ArrayList<Song>) {
                refreshCache(songs)
                refreshLocalDataSource(songs)
                callback.onSongsLoaded(ArrayList(cachedSongs.values))
            }

            override fun onDataNotAvailable() {
                callback.onDataNotAvailable()
            }
        })
    }

    private fun refreshLocalDataSource(songs: ArrayList<Song>) {
        songsLocalDataSource.deleteAllSongs()
        for (song in songs) {
            songsLocalDataSource.saveSong(song)
        }
    }

    private fun refreshCache(songs: ArrayList<Song>) {
        cachedSongs.clear()
        songs.forEach {
            cacheSong(it) {}
        }
        cacheIsDirty = false
    }

    private fun cacheSong(song: Song, perform: (Song) -> Unit) {
        cachedSongs[song.id] = song
        perform(song)
    }

    companion object {

        private var INSTANCE: SongsRepository? = null

        @JvmStatic
        fun getInstance(songsRemoteDataSource: SongsDataSource,
                        songsLocalDataSource: SongsDataSource) =
                INSTANCE ?: synchronized(SongsRepository::class.java) {
                    INSTANCE ?: SongsRepository(songsRemoteDataSource, songsLocalDataSource)
                            .also { INSTANCE = it }
                }


        /**
         * Used to force [getInstance] to create a new instance
         * next time it's called.
         */
        @JvmStatic
        fun destroyInstance() {
            INSTANCE = null
        }
    }

}