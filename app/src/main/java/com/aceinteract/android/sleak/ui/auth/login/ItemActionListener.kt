package com.aceinteract.android.sleak.ui.auth.login

interface ItemActionListener {

    fun onLoginClicked()

}