package com.aceinteract.android.sleak.data.model

class AccountList : ArrayList<Account>() {

    override fun toString(): String {
        var output = ""
        var index = 0
        forEach {
            output += (if (index == 0) it.fullName else ", ${it.fullName}")
            index++
        }
        return output
    }

    fun toStringArray(): ArrayList<String> {
        val output = ArrayList<String>()
        forEach {
            output.add(it.fullName)
        }
        return output
    }

}