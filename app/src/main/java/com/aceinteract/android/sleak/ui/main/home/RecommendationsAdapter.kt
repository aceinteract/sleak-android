package com.aceinteract.android.sleak.ui.main.home

import android.content.Context
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.aceinteract.android.sleak.data.model.Recommendation
import com.aceinteract.android.sleak.databinding.ItemRecommendationBinding

class RecommendationsAdapter(
        private var context: Context,
        private var recommendations: ArrayList<Recommendation>,
        private val viewModel: HomeViewModel
) : RecyclerView.Adapter<RecommendationsAdapter.RecommendationsViewHolder>() {

    private lateinit var binding: ItemRecommendationBinding

    fun replaceData(recommendations: ArrayList<Recommendation>) {
        this.recommendations = recommendations
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecommendationsViewHolder{
        val inflater = LayoutInflater.from(parent.context)
        binding = ItemRecommendationBinding.inflate(inflater, parent, false)

        return RecommendationsViewHolder(binding.root)
    }

    override fun getItemCount(): Int = recommendations.size

    override fun onBindViewHolder(holder: RecommendationsViewHolder, position: Int) {

        with(binding) {
            recommendation = recommendations[position]
            viewModel = this@RecommendationsAdapter.viewModel
        }

        when (recommendations[position].type) {
            Recommendation.TYPE_ALBUM -> {
                val itemRecommendationsAdapter = AlbumRecommendationsAdapter(recommendations[position].albums,
                        recommendations[position].type, viewModel)
                binding.rvRecommendedAlbums.adapter = itemRecommendationsAdapter
                binding.rvRecommendedAlbums.layoutManager = GridLayoutManager(
                        context,
                        1,
                        GridLayoutManager.HORIZONTAL,
                        false
                )
            }
            Recommendation.TYPE_PLAYLIST ->  {
                val itemRecommendationsAdapter = PlaylistRecommendationsAdapter(recommendations[position].playlists,
                        recommendations[position].type, viewModel)
                binding.rvRecommendedPlaylists.adapter = itemRecommendationsAdapter
                binding.rvRecommendedPlaylists.layoutManager = GridLayoutManager(
                        context,
                        1,
                        GridLayoutManager.HORIZONTAL,
                        false
                )
            }
            else -> throw IllegalArgumentException("Item type must be either Recommendation.TYPE_ALBUM" +
                    " or Recommendation.TYPE_PLAYLIST")
        }

    }

    class RecommendationsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

}

