package com.aceinteract.android.sleak.data.remote.api

import com.aceinteract.android.sleak.data.model.Account
import com.aceinteract.android.sleak.data.model.Album
import com.aceinteract.android.sleak.data.model.Recommendation
import retrofit2.Call
import retrofit2.http.*

interface ApiService {

    @GET("home-recommendations/{userId}/")
    fun getHomeRecommendations(@Path("userId") userId: String): Call<ArrayList<Recommendation>>

    @GET("album/{albumId}/")
    fun getAlbum(@Path("albumId") albumId: String): Call<Album>

    @POST("library/albums/add/{libraryId}")
    @FormUrlEncoded
    fun addAlbumToLibrary(@Path("libraryId") libraryId: String, @Field("albumIds") albumIds: ArrayList<String>): Call<String>

    @POST("library/songs/add/{libraryId}")
    @FormUrlEncoded
    fun addSongToLibrary(@Path("libraryId") libraryId: String, @Field("songIds") songIds: ArrayList<String>): Call<String>

    @POST("account/login")
    @FormUrlEncoded
    fun loginAccount(@Field("username") username: String, @Field("password") password: String): Call<Account>

    @GET("account/{userId}")
    fun getAccount(@Path("userId") userId: String): Call<Account>

    @POST("account/update")
    @FormUrlEncoded
    fun updateAccount(@Field("account") account: Account): Call<Account>

}