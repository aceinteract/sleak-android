package com.aceinteract.android.sleak.ui.main.home

import android.databinding.ViewDataBinding
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.aceinteract.android.sleak.data.model.Album
import com.aceinteract.android.sleak.data.model.Playlist
import com.aceinteract.android.sleak.data.model.Recommendation
import com.aceinteract.android.sleak.databinding.ItemRecommendedAlbumBinding
import com.aceinteract.android.sleak.databinding.ItemRecommendedPlaylistBinding

abstract class ItemRecommendationsAdapter<T>(
        private var itemList: ArrayList<T>,
        private var itemType: Int
) : RecyclerView.Adapter<ItemRecommendationsAdapter.ItemRecommendationsViewHolder>() {

    lateinit var itemBinding: ViewDataBinding

    fun replaceData(itemList: ArrayList<T>) {
        this.itemList = itemList
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemRecommendationsViewHolder {

        val inflater = LayoutInflater.from(parent.context)

        itemBinding = when (itemType) {
            Recommendation.TYPE_ALBUM -> ItemRecommendedAlbumBinding.inflate(inflater, parent, false)
            Recommendation.TYPE_PLAYLIST -> ItemRecommendedPlaylistBinding.inflate(inflater, parent, false)
            else -> throw IllegalArgumentException("Item type must be either Recommendation.TYPE_ALBUM" +
                    " or Recommendation.TYPE_PLAYLIST")
        }
        return ItemRecommendationsViewHolder(itemBinding.root)

    }

    override fun getItemCount(): Int = itemList.size

    abstract override fun onBindViewHolder(holder: ItemRecommendationsViewHolder, position: Int)

    class ItemRecommendationsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

}

class AlbumRecommendationsAdapter(
        private var itemList: ArrayList<Album>,
        itemType: Int,
        private var viewModel: HomeViewModel
) : ItemRecommendationsAdapter<Album>(itemList, itemType) {

    override fun onBindViewHolder(holder: ItemRecommendationsViewHolder, position: Int) {

        val itemActionListener = object : AlbumItemActionListener {
            override fun onAlbumClicked(album: Album) {
                viewModel.openAlbumEvent.value = album.id
            }
        }
        with(itemBinding as ItemRecommendedAlbumBinding) {
            listener = itemActionListener
            album = itemList[position]
        }

    }

}

class PlaylistRecommendationsAdapter(
        private var itemList: ArrayList<Playlist>,
        itemType: Int,
        private var viewModel: HomeViewModel
) : ItemRecommendationsAdapter<Playlist>(itemList, itemType) {

    override fun onBindViewHolder(holder: ItemRecommendationsViewHolder, position: Int) {

        val itemActionListener = object : PlaylistItemActionListener {
            override fun onPlaylistClicked(playlist: Playlist) {
                viewModel.openPlaylistEvent.value = playlist.id
            }
        }
        with(itemBinding as ItemRecommendedPlaylistBinding) {
            listener = itemActionListener
            playlist = itemList[position]
        }

    }

}