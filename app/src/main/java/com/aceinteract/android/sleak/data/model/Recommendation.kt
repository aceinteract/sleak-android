package com.aceinteract.android.sleak.data.model

data class Recommendation(
        val label: String,
        val type: Int,
        val albums: ArrayList<Album> = ArrayList(),
        val playlists: ArrayList<Playlist> = ArrayList()
) {

    companion object {
        const val TYPE_ALBUM = 1
        const val TYPE_PLAYLIST = 2
    }

}