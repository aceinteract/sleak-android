package com.aceinteract.android.sleak.ui.main.album

import com.aceinteract.android.sleak.data.model.Album
import com.aceinteract.android.sleak.data.model.Song

interface ItemActionListener {

    fun onSongClicked(song: Song)

    fun onAlbumAddClicked(album: Album)

    fun onSongAddClicked(song: Song)

    fun onArtistClicked(userId: String)

}