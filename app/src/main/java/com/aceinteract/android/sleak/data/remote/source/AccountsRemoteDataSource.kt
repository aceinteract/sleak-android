package com.aceinteract.android.sleak.data.remote.source

import com.aceinteract.android.sleak.data.model.Account
import com.aceinteract.android.sleak.data.remote.api.ApiCallback
import com.aceinteract.android.sleak.data.remote.api.ApiClient
import com.aceinteract.android.sleak.data.source.AccountsDataSource
import com.aceinteract.android.sleak.util.AppExecutors
import retrofit2.Call
import retrofit2.Response

class AccountsRemoteDataSource(val appExecutors: AppExecutors) : AccountsDataSource {

    private val apiClient = ApiClient()

    override fun loginAccount(username: String, password: String, callback: AccountsDataSource.LoginCallback) {
        appExecutors.networkIO.execute {

            apiClient.loginAccount(username, password, object : ApiCallback<Account> {
                override fun onFail(call: Call<Account>?, t: Throwable?) {
                    appExecutors.mainThread.execute { callback.onLoginFailure(Account.LoginFailureReason.SERVER_ERROR) }
                }

                override fun onResponse(call: Call<Account>?, response: Response<Account>?) {
                    appExecutors.mainThread.execute {
                        when (response!!.code()) {
                            200 -> callback.onLoginSuccess(response.body()!!)
                            404 -> callback.onLoginFailure(Account.LoginFailureReason.USER_NOT_FOUND)
                            401 -> callback.onLoginFailure(Account.LoginFailureReason.INVALID_PASSWORD)
                            201 -> callback.onLoginFailure(Account.LoginFailureReason.USER_BLOCKED)
                            else -> callback.onLoginFailure(Account.LoginFailureReason.SERVER_ERROR)
                        }
                    }
                }
            })

        }
    }

    override fun getAccount(accountId: String, callback: AccountsDataSource.GetAccountCallback) {
        appExecutors.networkIO.execute {

            apiClient.getAccount(accountId, object : ApiCallback<Account> {

                override fun onFail(call: Call<Account>?, t: Throwable?) {
                    appExecutors.mainThread.execute { callback.onDataNotAvailable() }
                }

                override fun onResponse(call: Call<Account>?, response: Response<Account>?) {
                    appExecutors.mainThread.execute {
                        when (response!!.code()) {
                            200 -> callback.onAccountLoaded(response.body()!!)
                            else -> callback.onDataNotAvailable()
                        }
                    }
                }

            })

        }
    }

    override fun updateAccount(account: Account, callback: AccountsDataSource.UpdateAccountCallback) {
        appExecutors.networkIO.execute {

            apiClient.updateAccount(account, object : ApiCallback<Account> {

                override fun onFail(call: Call<Account>?, t: Throwable?) {
                    appExecutors.mainThread.execute { callback.onDataNotAvailable() }
                }

                override fun onResponse(call: Call<Account>?, response: Response<Account>?) {
                    appExecutors.mainThread.execute {
                        when (response!!.code()) {
                            200 -> callback.onAccountUpdated(response.body()!!)
                            else -> callback.onDataNotAvailable()
                        }
                    }
                }

            })

        }
    }

}