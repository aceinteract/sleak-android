package com.aceinteract.android.sleak.ui.songs

import android.annotation.SuppressLint
import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.content.Context
import android.databinding.ObservableArrayList
import android.databinding.ObservableBoolean
import com.aceinteract.android.sleak.SingleLiveEvent
import com.aceinteract.android.sleak.data.entity.Song
import com.aceinteract.android.sleak.data.source.SongsDataSource
import com.aceinteract.android.sleak.data.repository.SongsRepository

class SongsViewModel(context: Application, private val songsRepository: SongsRepository) : AndroidViewModel(context) {

    private val isDataLoadingError = ObservableBoolean(false)
    @SuppressLint("StaticFieldLeak")
    private val context: Context = context.applicationContext

    internal val playSongEvent = SingleLiveEvent<String>()

    val items: ObservableArrayList<Song> = ObservableArrayList()
    val dataLoading = ObservableBoolean(false)
    val empty = ObservableBoolean(false)
    val snackbarMessage = SingleLiveEvent<Int>()

    fun start() {
        loadSongs(false)
    }

    fun loadSongs(forceUpdate: Boolean) {
        loadSongs(forceUpdate, false)
    }

    fun loadSongs(forceUpdate: Boolean, showLoadingUI: Boolean) {
        if (showLoadingUI) {
            dataLoading.set(true)
        }
        if (forceUpdate) {
            songsRepository.refreshSongs()
        }

        songsRepository.getSongs(object : SongsDataSource.LoadSongsCallback {
            override fun onSongsLoaded(songs: ArrayList<Song>) {
                if (showLoadingUI) {
                    dataLoading.set(false)
                }

                isDataLoadingError.set(false)

                with(items) {
                    clear()
                    addAll(songs)
                    empty.set(isEmpty())
                }
            }

            override fun onDataNotAvailable() {
                isDataLoadingError.set(true)
            }
        })
    }

    fun showSnackbarMessage(message: Int) {
        snackbarMessage.value = message
    }

}