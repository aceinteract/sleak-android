package com.aceinteract.android.sleak.data.repository

import com.aceinteract.android.sleak.data.model.Account
import com.aceinteract.android.sleak.data.remote.source.AccountsRemoteDataSource
import com.aceinteract.android.sleak.data.source.AccountsDataSource
import com.aceinteract.android.sleak.util.StorageUtil

class AccountsRepository(private val storageUtil: StorageUtil,
                         private val accountsRemoteDataSource: AccountsRemoteDataSource) : BaseRepository(), AccountsDataSource {

    fun logoutAccount() {
        storageUtil.clearStorage()
    }

    override fun loginAccount(username: String, password: String, callback: AccountsDataSource.LoginCallback) {
        accountsRemoteDataSource.loginAccount(username, password, object : AccountsDataSource.LoginCallback {
            override fun onLoginSuccess(account: Account) {
                val library = account.library
                account.library = null

                storageUtil.library = library
                storageUtil.currentUser = account

                account.library = library

                callback.onLoginSuccess(account)
            }

            override fun onLoginFailure(reason: Int) {
                callback.onLoginFailure(reason)
            }
        })
    }

    override fun getAccount(accountId: String, callback: AccountsDataSource.GetAccountCallback) {
        accountsRemoteDataSource.getAccount(accountId, object : AccountsDataSource.GetAccountCallback {
            override fun onAccountLoaded(account: Account) {
                callback.onAccountLoaded(account)
            }

            override fun onDataNotAvailable() {
                callback.onDataNotAvailable()
            }
        })
    }

    override fun updateAccount(account: Account, callback: AccountsDataSource.UpdateAccountCallback) {
        accountsRemoteDataSource.updateAccount(account, object : AccountsDataSource.UpdateAccountCallback {
            override fun onAccountUpdated(account: Account) {
                callback.onAccountUpdated(account)
            }

            override fun onDataNotAvailable() {
                callback.onDataNotAvailable()
            }
        })
    }

    companion object {

        private var INSTANCE: AccountsRepository? = null

        @JvmStatic
        fun getInstance(storageUtil: StorageUtil, accountsRemoteDataSource: AccountsRemoteDataSource) =
                INSTANCE ?: synchronized(AccountsRepository::class.java) {
                    INSTANCE
                            ?: AccountsRepository(storageUtil, accountsRemoteDataSource).also { INSTANCE = it }
                }

        @JvmStatic
        fun destroyInstance() {
            INSTANCE = null
        }

    }

}