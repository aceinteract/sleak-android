package com.aceinteract.android.sleak.ui.songs

import android.arch.lifecycle.Observer
import android.os.Bundle
import com.aceinteract.android.sleak.R
import com.aceinteract.android.sleak.ui.base.BaseActivity

class SongsActivity : BaseActivity() {

    private lateinit var viewModel: SongsViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_songs)

        setupViewFragment()

        viewModel = obtainViewModel().apply {
            playSongEvent.observe(this@SongsActivity, Observer {
                if (it != null) {
                    playSong()
                }
            })
        }
    }

    private fun playSong() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


    private fun setupViewFragment() {
        supportFragmentManager.findFragmentById(R.id.frame_container) ?:
                SongsFragment.newInstance().let {
                    replaceFragmentInActivity(it, R.id.frame_container)
                }
    }

    fun obtainViewModel(): SongsViewModel = obtainViewModel(SongsViewModel::class.java)
}
