package com.aceinteract.android.sleak.ui.songs

import com.aceinteract.android.sleak.data.model.Song

interface SongItemActionListener {

    fun onSongClicked(song: Song)

}