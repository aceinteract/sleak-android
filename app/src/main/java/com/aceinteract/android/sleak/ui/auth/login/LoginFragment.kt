package com.aceinteract.android.sleak.ui.auth.login

import android.arch.lifecycle.Observer
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.aceinteract.android.sleak.SleakApplication
import com.aceinteract.android.sleak.databinding.FragmentLoginBinding
import com.aceinteract.android.sleak.ui.base.BaseFragment
import com.aceinteract.android.sleak.ui.main.MainActivity
import com.aceinteract.android.sleak.util.setupSnackbar

class LoginFragment : BaseFragment() {

    private lateinit var viewDataBinding: FragmentLoginBinding

    val listener = object : ItemActionListener {
        override fun onLoginClicked() {
            viewDataBinding.viewModel!!.attemptLogin()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewDataBinding = FragmentLoginBinding.inflate(inflater, container, false).apply {
            viewModel = obtainViewModel(LoginViewModelFactory.getInstance(activity!!.application as SleakApplication))
            listener = this@LoginFragment.listener
        }
        return viewDataBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewDataBinding.viewModel?.let {
            view?.setupSnackbar(this, it.snackbarMessage, Snackbar.LENGTH_LONG)
        }
        viewDataBinding.viewModel?.apply {
            loggedInEvent.observe(this@LoginFragment, Observer {
                Toast.makeText(context, "Welcome back, ${it?.fullName}!", Toast.LENGTH_LONG).show()
                val mainIntent = Intent(context, MainActivity::class.java)
                mainIntent.putExtra(IS_FIRST_TIME, true)
                startActivity(mainIntent)
                activity!!.finish()
            })
        }
    }

    companion object {
        const val IS_FIRST_TIME = "is_first_time"
    }

}