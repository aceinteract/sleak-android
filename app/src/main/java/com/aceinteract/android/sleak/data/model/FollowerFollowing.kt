package com.aceinteract.android.sleak.data.model

import android.arch.persistence.room.Entity

@Entity(tableName = "follower_following")
data class FollowerFollowing(
        val id: String,
        val follower: Account,
        val following: Account
)