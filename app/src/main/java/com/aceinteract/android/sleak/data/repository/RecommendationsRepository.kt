package com.aceinteract.android.sleak.data.repository

import com.aceinteract.android.sleak.data.model.Recommendation
import com.aceinteract.android.sleak.data.remote.source.RecommendationsRemoteDataSource
import com.aceinteract.android.sleak.data.source.RecommendationsDataSource

class RecommendationsRepository(private val recommendationsRemoteDataSource: RecommendationsRemoteDataSource) : BaseRepository(), RecommendationsDataSource {

    override fun getRecommendations(callback: RecommendationsDataSource.LoadRecommendationsCallback) {

        getRecommendationsFromRemote(callback)
    }

    private fun getRecommendationsFromRemote(callback: RecommendationsDataSource.LoadRecommendationsCallback) {
        recommendationsRemoteDataSource.getRecommendations(object : RecommendationsDataSource.LoadRecommendationsCallback {
            override fun onRecommendationsLoaded(recommendations: ArrayList<Recommendation>) {
                callback.onRecommendationsLoaded(recommendations)
            }

            override fun onDataNotAvailable() {
                callback.onDataNotAvailable()
            }
        })
    }

    companion object {

        private var INSTANCE: RecommendationsRepository? = null

        @JvmStatic
        fun getInstance(recommendationsRemoteDataSource: RecommendationsRemoteDataSource) =
                INSTANCE
                        ?: synchronized(RecommendationsRepository::class.java) {
                    INSTANCE
                            ?: RecommendationsRepository(recommendationsRemoteDataSource).also { INSTANCE = it }
                }

        @JvmStatic
        fun destroyInstance() {
            INSTANCE = null
        }

    }

}