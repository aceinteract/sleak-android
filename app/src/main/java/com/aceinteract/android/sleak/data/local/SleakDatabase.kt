package com.aceinteract.android.sleak.data.local

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import com.aceinteract.android.sleak.data.entity.Song
import com.aceinteract.android.sleak.data.local.dao.SongsDataAccessObject

@Database(entities = [(Song::class)], version = 1, exportSchema = false)
abstract class SleakDatabase : RoomDatabase() {

    abstract fun songsDataAccessObject(): SongsDataAccessObject

    companion object {

        private var INSTANCE: SleakDatabase? = null

        private val lock = Any()

        fun getInstance(context: Context): SleakDatabase {
            synchronized(lock) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.applicationContext,
                            SleakDatabase::class.java, "Sleak.db")
                            .build()
                }
                return INSTANCE!!
            }
        }

    }

}