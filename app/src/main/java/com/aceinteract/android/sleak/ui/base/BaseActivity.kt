package com.aceinteract.android.sleak.ui.base

import android.annotation.TargetApi
import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentTransaction
import android.support.v7.app.AppCompatActivity
import com.aceinteract.android.sleak.R
import com.aceinteract.android.sleak.SleakApplication
import com.aceinteract.android.sleak.ui.songs.SongsViewModelFactory
import com.aceinteract.android.sleak.util.NetworkUtil
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper


abstract class BaseActivity : AppCompatActivity() {

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase))
    }

    @TargetApi(Build.VERSION_CODES.M)
    fun hasPermission(permission: String): Boolean {
        return Build.VERSION.SDK_INT < Build.VERSION_CODES.M || checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED
    }

    fun isNetworkConnected(): Boolean {
        return NetworkUtil.isNetworkConnected(applicationContext)
    }

    fun openFragment(fragment: Fragment, fragmentManager: FragmentManager, tag: String) {
        if (fragmentManager.fragments[fragmentManager.backStackEntryCount - 1].tag != tag) {
            fragmentManager.transact {
                setCustomAnimations(
                        R.anim.anim_slide_left_enter,
                        R.anim.anim_slide_left_exit,
                        R.anim.anim_slide_right_enter,
                        R.anim.anim_slide_right_exit
                )
                add(R.id.frame_container, fragment, tag)
                addToBackStack(tag)
            }
        }
    }


    fun <T : ViewModel> obtainViewModel(viewModelClass: Class<T>) =
            ViewModelProviders.of(this, SongsViewModelFactory.getInstance(application as SleakApplication)).get(viewModelClass)

    fun replaceFragmentInActivity(fragment: Fragment, frameId: Int) {
        supportFragmentManager.transact {
            replace(frameId, fragment)
        }
    }

    inline fun FragmentManager.transact(action: FragmentTransaction.() -> Unit) {
        beginTransaction().apply {
            action()
        }.commit()
    }

}