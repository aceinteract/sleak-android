package com.aceinteract.android.sleak.data.remote.source

import com.aceinteract.android.sleak.data.remote.api.ApiClient
import com.aceinteract.android.sleak.data.source.LibraryDataSource
import com.aceinteract.android.sleak.util.AppExecutors

class LibraryRemoteDataSource(private val appExecutors: AppExecutors) : LibraryDataSource {

    private val apiClient = ApiClient()

    override fun addSongsToLibrary(libraryId: String, songIds: ArrayList<String>) {
        appExecutors.networkIO.execute {

            apiClient.addSongsToLibrary(libraryId, songIds)

        }
    }

    override fun addAlbumsToLibrary(libraryId: String, albumIds: ArrayList<String>) {
        appExecutors.networkIO.execute {

            apiClient.addAlbumsToLibrary(libraryId, albumIds)

        }
    }

}