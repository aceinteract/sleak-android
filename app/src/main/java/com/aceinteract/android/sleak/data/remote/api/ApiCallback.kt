package com.aceinteract.android.sleak.data.remote.api

import retrofit2.Call
import retrofit2.Response

interface ApiCallback<T> {

    fun onFail(call: Call<T>?, t: Throwable?)

    fun onResponse(call: Call<T>?, response: Response<T>?)

}