package com.aceinteract.android.sleak.data.remote.api

import android.util.Log
import com.aceinteract.android.sleak.SleakApplication
import com.aceinteract.android.sleak.data.model.Account
import com.aceinteract.android.sleak.data.model.Album
import com.aceinteract.android.sleak.data.model.Recommendation
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class ApiClient {

    private val apiBaseUrl = "${SleakApplication.baseUrl}api/v1/"

    private val httpClient = OkHttpClient.Builder()

    private val builder = Retrofit.Builder()
            .baseUrl(apiBaseUrl)
            .addConverterFactory(GsonConverterFactory.create())

    private var retrofit: Retrofit

    private var apiService: ApiService

    init {
        retrofit = builder.client(httpClient.build()).build()
        apiService = retrofit.create(ApiService::class.java)
    }

    fun getHomeRecommendations(userId: String, callback: ApiCallback<ArrayList<Recommendation>>) {

        val requestCall = apiService.getHomeRecommendations(userId)
        requestCall.enqueue(object : Callback<ArrayList<Recommendation>> {

            override fun onFailure(call: Call<ArrayList<Recommendation>>?, t: Throwable?) {
                Log.e(this@ApiClient::getHomeRecommendations.name, t!!.message)
                callback.onFail(call, t)
            }

            override fun onResponse(call: Call<ArrayList<Recommendation>>?, response: Response<ArrayList<Recommendation>>?) {
                callback.onResponse(call, response)
                if (response!!.errorBody() != null) {
                    Log.e(this@ApiClient::getHomeRecommendations.name, response.errorBody()!!.string())
                }
            }

        })

    }

    fun getAlbum(albumId: String, callback: ApiCallback<Album>) {

        val requestCall = apiService.getAlbum(albumId)
        requestCall.enqueue(object : Callback<Album> {

            override fun onFailure(call: Call<Album>?, t: Throwable?) {
                Log.e(this@ApiClient::getAlbum.name, t!!.message)
                callback.onFail(call, t)
            }

            override fun onResponse(call: Call<Album>?, response: Response<Album>?) {
                callback.onResponse(call, response)
                if (response!!.errorBody() != null) {
                    Log.e(this@ApiClient::getAlbum.name, response.errorBody()!!.string())
                }
            }

        })

    }

    fun addSongsToLibrary(libraryId: String, songId: ArrayList<String>) {

        val requestCall = apiService.addSongToLibrary(libraryId, songId)
        requestCall.enqueue(object : Callback<String> {

            override fun onFailure(call: Call<String>?, t: Throwable?) {
                Log.e(this@ApiClient::addSongsToLibrary.name, t?.message)
            }

            override fun onResponse(call: Call<String>?, response: Response<String>?) {
                if (response!!.errorBody() != null) {
                    Log.e(this@ApiClient::addSongsToLibrary.name, response.errorBody()?.string())
                }
                if (response.code() != 200 || response.body() != "Done")
                    response.body()?.let { Log.e(this@ApiClient::addSongsToLibrary.name, it) }
            }

        })

    }

    fun addAlbumsToLibrary(libraryId: String, albumIds: ArrayList<String>) {

        val requestCall = apiService.addAlbumToLibrary(libraryId, albumIds)
        requestCall.enqueue(object : Callback<String> {

            override fun onFailure(call: Call<String>?, t: Throwable?) {
                Log.e(this@ApiClient::addAlbumsToLibrary.name, t?.message)
            }

            override fun onResponse(call: Call<String>?, response: Response<String>?) {
                if (response!!.errorBody() != null) {
                    Log.e(this@ApiClient::addAlbumsToLibrary.name, response.errorBody()?.string())
                }
                if (response.code() != 200 || response.body() != "Done")
                    response.body()?.let { Log.e(this@ApiClient::addAlbumsToLibrary.name, it) }
            }

        })

    }

    fun loginAccount(username: String, password: String, callback: ApiCallback<Account>) {

        val requestCall = apiService.loginAccount(username, password)
        requestCall.enqueue(object : Callback<Account> {

            override fun onFailure(call: Call<Account>?, t: Throwable?) {
                Log.e(this@ApiClient::loginAccount.name, t!!.message)
                callback.onFail(call, t)
            }

            override fun onResponse(call: Call<Account>?, response: Response<Account>?) {
                callback.onResponse(call, response)
                if (response!!.errorBody() != null) {
                    Log.e(this@ApiClient::loginAccount.name, response.errorBody()!!.string())
                }
            }

        })

    }

    fun getAccount(userId: String, callback: ApiCallback<Account>) {

        val requestCall = apiService.getAccount(userId)
        requestCall.enqueue(object : Callback<Account> {

            override fun onFailure(call: Call<Account>?, t: Throwable?) {
                Log.e(this@ApiClient::getAccount.name, t!!.message)
                callback.onFail(call, t)
            }

            override fun onResponse(call: Call<Account>?, response: Response<Account>?) {
                callback.onResponse(call, response)
                if (response!!.errorBody() != null) {
                    Log.e(this@ApiClient::getAccount.name, response.errorBody()!!.string())
                }
            }

        })

    }

    fun updateAccount(account: Account, callback: ApiCallback<Account>) {

        val requestCall = apiService.updateAccount(account)
        requestCall.enqueue(object : Callback<Account> {

            override fun onFailure(call: Call<Account>?, t: Throwable?) {
                Log.e(this@ApiClient::updateAccount.name, t!!.message)
                callback.onFail(call, t)
            }

            override fun onResponse(call: Call<Account>?, response: Response<Account>?) {
                callback.onResponse(call, response)
                if (response!!.errorBody() != null) {
                    Log.e(this@ApiClient::updateAccount.name, response.errorBody()!!.string())
                }
            }

        })

    }

}