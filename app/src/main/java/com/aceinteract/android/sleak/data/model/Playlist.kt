package com.aceinteract.android.sleak.data.model

import android.arch.persistence.room.Entity

data class Playlist(
        val id: String,
        val title: String,
        val description: String,
        val songs: List<Song>,
        val creator: Account
)