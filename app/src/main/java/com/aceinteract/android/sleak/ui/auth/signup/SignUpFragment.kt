package com.aceinteract.android.sleak.ui.auth.signup

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.aceinteract.android.sleak.databinding.FragmentSignUpBinding
import com.aceinteract.android.sleak.ui.base.BaseFragment


class SignUpFragment : BaseFragment() {

    private lateinit var viewDataBinding: FragmentSignUpBinding

//    val listener = object : ItemActionListener {
//        override fun onLoginClicked() {
//            viewDataBinding.viewModel!!.attemptLoginEvent.call()
//        }
//    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewDataBinding = FragmentSignUpBinding.inflate(inflater, container, false).apply {
//            viewModel = obtainViewModel(LoginViewModelFactory.getInstance(activity!!.application as SleakApplication))
//            listener = this@LoginFragment.listener
        }
        return viewDataBinding.root
    }

//    override fun onActivityCreated(savedInstanceState: Bundle?) {
//        super.onActivityCreated(savedInstanceState)
//        viewDataBinding.viewModel?.let {
//            view?.setupSnackbar(this, it.snackbarMessage, Snackbar.LENGTH_LONG)
//        }
//        viewDataBinding.viewModel?.apply {
//            attemptLoginEvent.observe(this@LoginFragment, Observer {
//                attemptLogin()
//            })
//        }
//    }

}