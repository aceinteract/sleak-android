package com.aceinteract.android.sleak.data.source

import com.aceinteract.android.sleak.data.model.Account

interface AccountsDataSource {

    interface LoginCallback {

        fun onLoginSuccess(account: Account)

        fun onLoginFailure(@Account.LoginFailureReason.LoginFailureReasonInt reason: Int)
    }

    interface GetAccountCallback : DataSourceCallback {

        fun onAccountLoaded(account: Account)

    }

    interface UpdateAccountCallback : DataSourceCallback {

        fun onAccountUpdated(account: Account)

    }

    fun loginAccount(username: String, password: String, callback: LoginCallback)

    fun getAccount(accountId: String, callback: GetAccountCallback)

    fun updateAccount(account: Account, callback: UpdateAccountCallback)

}