package com.aceinteract.android.sleak.data.local.source

import android.support.annotation.VisibleForTesting
import com.aceinteract.android.sleak.data.entity.Song
import com.aceinteract.android.sleak.data.local.dao.SongsDataAccessObject
import com.aceinteract.android.sleak.data.source.SongsDataSource
import com.aceinteract.android.sleak.util.AppExecutors

class SongsLocalDataSource private constructor(val appExecutors: AppExecutors, val songsDataAccessObject: SongsDataAccessObject) : SongsDataSource {

    override fun getSongs(callback: SongsDataSource.LoadSongsCallback) {
        appExecutors.diskIO.execute {
            val songs = songsDataAccessObject.getSongs()
            appExecutors.mainThread.execute {
                if (songs.isEmpty()) {
                    callback.onDataNotAvailable()
                } else {
                    callback.onSongsLoaded(songs as ArrayList<Song>)
                }
            }
        }
    }

    override fun getSong(songId: String, callback: SongsDataSource.GetSongCallback) {
        appExecutors.diskIO.execute {
            val song = songsDataAccessObject.getSongById(songId)
            appExecutors.mainThread.execute {
                if (song != null) {
                    callback.onSongLoaded(song)
                } else {
                    callback.onDataNotAvailable()
                }
            }
        }
    }

    override fun saveSong(song: Song) {
        appExecutors.diskIO.execute { songsDataAccessObject.insertSong(song) }
    }

    override fun refreshSongs() {
        // Not needed
    }

    override fun deleteAllSongs() {
        appExecutors.diskIO.execute { songsDataAccessObject.deleteAllSongs() }
    }

    override fun deleteSong(songId: String) {
        appExecutors.diskIO.execute { songsDataAccessObject.deleteSongById(songId) }
    }

    companion object {
        private var INSTANCE: SongsLocalDataSource? = null

        @JvmStatic
        fun getInstance(appExecutors: AppExecutors, songsDataAccessObject: SongsDataAccessObject): SongsLocalDataSource {
            if (INSTANCE == null) {
                synchronized(SongsLocalDataSource::javaClass) {
                    INSTANCE = SongsLocalDataSource(appExecutors, songsDataAccessObject)
                }
            }
            return INSTANCE!!
        }

        @VisibleForTesting
        fun clearInstance() {
            INSTANCE = null
        }
    }

}