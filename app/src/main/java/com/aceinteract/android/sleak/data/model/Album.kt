package com.aceinteract.android.sleak.data.model

import java.util.*
import kotlin.collections.ArrayList

data class Album(
        val id: String,
        val title: String,
        val dateReleased: Date,
        val artists: ArrayList<Account> = ArrayList(),
        val songs: ArrayList<Song> = ArrayList(),
        val description: String = "",
        val genre: String = GENRE_NA
) {

    val albumType: Int
        get() {
            return if (songs.size > 1) TYPE_ALBUM else TYPE_SINGLE
        }

    fun getStringArtists(): String = artists.joinToString()

    companion object {

        const val TYPE_ALBUM = 1
        const val TYPE_SINGLE = 2

        const val GENRE_NA = "NA"
        const val GENRE_AFRO_BEAT = "AFRO_BEAT"
        const val GENRE_CLASSICAL = "CLASSICAL"
        const val GENRE_COUNTRY = "COUNTRY"
        const val GENRE_DANCE_AND_EDM = "DANCE_AND_EDM"
        const val GENRE_DUBSTEP = "DUBSTEP"
        const val GENRE_FOLK = "FOLK"
        const val GENRE_HIGH_LIFE = "HIGH_LIFE"
        const val GENRE_HIP_HOP_RAP = "HIP_HOP_RAP"
        const val GENRE_HOUSE = "HOUSE"
        const val GENRE_INDIE = "INDIE"
        const val GENRE_JAZZ_AND_BLUES = "JAZZ_AND_BLUES"
        const val GENRE_METAL = "METAL"
        const val GENRE_POP = "POP"
        const val GENRE_R_AND_B = "R_AND_B"
        const val GENRE_REGGAE = "REGGAE"
        const val GENRE_ROCK = "ROCK"
        const val GENRE_SOUL = "SOUL"

    }

}