package com.aceinteract.android.sleak.ui.main.home

import com.aceinteract.android.sleak.data.model.Album
import com.aceinteract.android.sleak.data.model.Playlist

interface AlbumItemActionListener {

    fun onAlbumClicked(album: Album)

}

interface PlaylistItemActionListener {

    fun onPlaylistClicked(playlist: Playlist)

}