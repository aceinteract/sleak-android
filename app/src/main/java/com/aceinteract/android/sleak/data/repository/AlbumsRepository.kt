package com.aceinteract.android.sleak.data.repository

import com.aceinteract.android.sleak.data.model.Album
import com.aceinteract.android.sleak.data.remote.source.AlbumsRemoteDataSource
import com.aceinteract.android.sleak.data.source.AlbumsDataSource

class AlbumsRepository(private val albumsRemoteDataSource: AlbumsRemoteDataSource) : BaseRepository(), AlbumsDataSource {

    override fun getAlbum(albumId: String, callback: AlbumsDataSource.GetAlbumCallback) {
        getAlbumFromRemote(albumId, callback)
    }

    private fun getAlbumFromRemote(albumId: String, callback: AlbumsDataSource.GetAlbumCallback) {
        albumsRemoteDataSource.getAlbum(albumId, object : AlbumsDataSource.GetAlbumCallback {

            override fun onAlbumLoaded(album: Album) {
                callback.onAlbumLoaded(album)
            }

            override fun onDataNotAvailable() {
                callback.onDataNotAvailable()
            }

        })
    }

    companion object {

        private var INSTANCE: AlbumsRepository? = null

        @JvmStatic
        fun getInstance(albumsRemoteDataSource: AlbumsRemoteDataSource) =
                INSTANCE
                        ?: synchronized(AlbumsRepository::class.java) {
                            INSTANCE
                                    ?: AlbumsRepository(albumsRemoteDataSource).also { INSTANCE = it }
                        }

        @JvmStatic
        fun destroyInstance() {
            INSTANCE = null
        }

    }


}