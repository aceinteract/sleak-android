package com.aceinteract.android.sleak.data.source

interface DataSourceCallback {

    fun onDataNotAvailable()

}