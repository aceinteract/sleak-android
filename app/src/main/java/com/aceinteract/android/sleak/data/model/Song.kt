package com.aceinteract.android.sleak.data.model

import java.util.*
import kotlin.collections.ArrayList

data class Song(
        val id: String,
        val title: String,
        val length: Long,
        var album: Album,
        val trackNumber: Int,
        val artists: AccountList = AccountList()
) {

    var artistIds = ArrayList<String>()
        get() {
            val ids = ArrayList<String>()
            artists.forEach {
                ids.add(it.id)
            }
            return ids
        }

    fun getLengthInMinutes(): String {
        val inSeconds = length / 1000.0
        val minutes = (inSeconds / 60.0).toInt()
        val seconds = (inSeconds % 60).toInt()
        return "$minutes:$seconds"
    }

    fun getStringArtists(): String = artists.joinToString()

    companion object {

        fun createSong() = Song("", "", 0,
                Album("", "", Date()), 0)

    }

}