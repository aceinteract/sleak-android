package com.aceinteract.android.sleak.ui.base

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import com.aceinteract.android.sleak.SingleLiveEvent
import com.aceinteract.android.sleak.util.StorageUtil

abstract class BaseViewModel(context: Application) : AndroidViewModel(context) {

    val storageUtil = StorageUtil(context)

    val snackbarMessage = SingleLiveEvent<Int>()

    fun showSnackbarMessage(message: Int) {
        snackbarMessage.value = message
    }

}