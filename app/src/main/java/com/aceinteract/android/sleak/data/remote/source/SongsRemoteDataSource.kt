package com.aceinteract.android.sleak.data.remote.source

import android.content.Context
import android.provider.MediaStore
import android.util.Log
import com.aceinteract.android.sleak.data.model.Album
import com.aceinteract.android.sleak.data.entity.Song
import com.aceinteract.android.sleak.data.source.SongsDataSource
import com.aceinteract.android.sleak.util.AppExecutors

class SongsRemoteDataSource(val context: Context, val appExecutors: AppExecutors) : SongsDataSource {

    override fun getSongs(callback: SongsDataSource.LoadSongsCallback) {
        appExecutors.diskIO.execute {

            val songs = ArrayList<Song>()

            val contentResolver = context.contentResolver

            val uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
            val selection = MediaStore.Audio.Media.IS_MUSIC + "!= 0"
            val sortOrder = MediaStore.Audio.Media.TITLE + " ASC"
            val cursor = contentResolver.query(uri, null, selection, null, sortOrder)


            if (cursor != null && cursor.count > 0) {
                while (cursor.moveToNext()) {
                    val id = cursor.getLong(cursor.getColumnIndex(MediaStore.Audio.Media._ID)).toString()
                    val title = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.TITLE))
                    val album = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ALBUM))
                    val artist = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ARTIST))

                    songs.add(Song(id, title, album, artist))
                }
            }

            cursor.close()

            appExecutors.mainThread.execute {

                if (songs.isEmpty()) {
                    callback.onDataNotAvailable()
                } else {
                    callback.onSongsLoaded(songs)
                }

            }

        }
    }

    override fun getSong(songId: String, callback: SongsDataSource.GetSongCallback) {
        // Not Needed
//        appExecutors.diskIO.execute {
//
//            var song: Song? = null
//
//            val contentResolver = application.applicationContext.contentResolver
//
//            val uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
//            val selection = MediaStore.Audio.Media._ID + " = " + songId
//            val cursor = contentResolver.query(uri, null, selection, null, null)
//
//            if (cursor != null && cursor.count > 0) {
//                while (cursor.moveToNext()) {
//                    val id = cursor.getLong(cursor.getColumnIndex(MediaStore.Audio.Media._ID)).toString()
//                    val title = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.TITLE))
//                    val album = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ALBUM))
//                    val artist = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ARTIST))
//
//                    song = Song(id, title, album, artist)
//                }
//            }
//
//            cursor.close()
//
//            appExecutors.mainThread.execute {
//                if (song != null) {
//                    callback.
//                }
//            }
//        }
    }

    override fun saveSong(song: Song) {
        // Not Needed
    }

    override fun refreshSongs() {
        // Not Needed
    }

    override fun deleteAllSongs() {
        // Not Needed
    }

    override fun deleteSong(songId: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}