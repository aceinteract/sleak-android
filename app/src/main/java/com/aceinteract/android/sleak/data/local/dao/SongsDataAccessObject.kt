package com.aceinteract.android.sleak.data.local.dao

import android.arch.persistence.room.*
import com.aceinteract.android.sleak.data.entity.Song

@Dao
interface SongsDataAccessObject {

    @Query("SELECT * FROM Songs")
    fun getSongs(): List<Song>

    @Query("SELECT * FROM Songs WHERE id = :songId")
    fun getSongById(songId: String): Song?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertSong(song: Song)

    @Update
    fun updateSong(song: Song): Int

    @Query("DELETE FROM Songs WHERE id = :songId")
    fun deleteSongById(songId: String): Int

    @Query("DELETE FROM Songs")
    fun deleteAllSongs()

}