package com.aceinteract.android.sleak.ui.main

import android.annotation.SuppressLint
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentTransaction
import android.widget.ImageView
import android.widget.TextView
import com.aceinteract.android.sleak.R
import com.aceinteract.android.sleak.SleakApplication
import com.aceinteract.android.sleak.ui.base.BaseActivity
import com.aceinteract.android.sleak.ui.main.home.HomeFragment
import com.aceinteract.android.sleak.util.StorageUtil
import com.aceinteract.android.sleak.util.disableShiftMode
import com.aceinteract.android.sleak.util.loadImageFromUrl
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity() {

    private lateinit var storageUtil: StorageUtil

    private var fragmentManager: FragmentManager? = null

    private lateinit var textSongTitle: TextView
    private lateinit var textSongArtist: TextView

    private var imageAlbumArt: ImageView? = null

    private var musicReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            textSongTitle.text = intent.extras.getString(SleakApplication.BROADCAST_TITLE)
            textSongArtist.text = intent.extras.getStringArrayList(SleakApplication.BROADCAST_ARTISTS).joinToString()
            imageAlbumArt!!.loadImageFromUrl("album/album-art/${intent.extras.getString(SleakApplication.BROADCAST_ALBUM_ID)}")
            changePlayPause(intent.extras.getBoolean(SleakApplication.BROADCAST_IS_PLAYING))
        }
    }

    @SuppressLint("CommitTransaction")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        storageUtil = StorageUtil(this)

        textSongTitle = text_song_title
        textSongArtist = text_song_artist
        imageAlbumArt = image_album_art

        setupMiniPlayer()

        setupButtons()

        setupBottomNavigation()

        setupFragments()
    }

    override fun onPause() {
        super.onPause()
        unregisterReceiver(musicReceiver)
    }

    override fun onResume() {
        super.onResume()
        registerReceiver(musicReceiver, IntentFilter(SleakApplication.MAIN_BROADCAST))
        setupMiniPlayer()
    }

    private fun setupMiniPlayer() {
        val queue = storageUtil.queue
        if (queue.isNotEmpty()) {
            val song = queue[storageUtil.currentQueuePosition]
            textSongTitle.text = song.title
            textSongArtist.text = song.artists.joinToString()
            imageAlbumArt!!.loadImageFromUrl("album/album-art/${song.album.id}")
        }
        changePlayPause(SleakApplication.INSTANCE!!.isSongPlaying)
    }

    private fun setupButtons() {
        button_play_pause.setOnClickListener {
            if (SleakApplication.INSTANCE!!.isSongPlaying) SleakApplication.INSTANCE!!.pauseSong()
            else SleakApplication.INSTANCE!!.startPlaying()
        }
    }

    @SuppressLint("CommitTransaction")
    private fun setupBottomNavigation() {
        val onNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.navigation_home -> {
                    openFragment(HomeFragment(), fragmentManager!!, HomeFragment.TAG)
                    return@OnNavigationItemSelectedListener true
                }
                R.id.navigation_explore -> {
                    //                message.setText(R.string.title_dashboard)
                    return@OnNavigationItemSelectedListener true
                }
                R.id.navigation_search -> {
                    //                message.setText(R.string.title_notifications)
                    return@OnNavigationItemSelectedListener true
                }
                R.id.navigation_me -> {
                    //                    openFragment(MyMusicFragment(), MY_MUSIC_FRAGMENT_TAG)
                    return@OnNavigationItemSelectedListener true
                }
            }
            false
        }

        navigation.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener)
        navigation.disableShiftMode()

    }

    @SuppressLint("CommitTransaction")
    private fun setupFragments() {

        fragmentManager = supportFragmentManager
        fragmentManager!!.addOnBackStackChangedListener {
            if (fragmentManager!!.backStackEntryCount > 0) {
                var fragmentTag = fragmentManager!!.fragments[fragmentManager!!.backStackEntryCount - 1].tag
                fragmentTag = fragmentTag!!.split("|")[0]
                val menu = when (fragmentTag) {
                    HomeFragment.TAG -> navigation.menu.findItem(R.id.navigation_home)
                    EXPLORE_FRAGMENT_TAG -> navigation.menu.findItem(R.id.navigation_explore)
                    SEARCH_FRAGMENT_TAG -> navigation.menu.findItem(R.id.navigation_search)
                    ME_FRAGMENT_TAG -> navigation.menu.findItem(R.id.navigation_me)
                    else -> navigation.menu.findItem(navigation.selectedItemId)
                }
                menu.isChecked = true
            } else {
                finish()
            }
        }

        fragmentManager!!.transact {
            replace(R.id.frame_container, HomeFragment(), HomeFragment.TAG)
            addToBackStack(HomeFragment.TAG)
            setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
        }

    }

    private fun changePlayPause(isPlaying: Boolean) {
        if (isPlaying) {
            button_play_pause.setImageResource(R.drawable.ic_pause_circular)
        } else {
            button_play_pause.setImageResource(R.drawable.ic_play_circular)
        }
    }

    companion object {

        const val EXPLORE_FRAGMENT_TAG = "exploreFragment"
        const val SEARCH_FRAGMENT_TAG = "searchFragment"
        const val ME_FRAGMENT_TAG = "meFragment"

    }
}
