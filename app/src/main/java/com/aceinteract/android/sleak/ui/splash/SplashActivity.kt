package com.aceinteract.android.sleak.ui.splash

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import com.aceinteract.android.sleak.R
import com.aceinteract.android.sleak.data.model.Account
import com.aceinteract.android.sleak.ui.auth.AuthActivity
import com.aceinteract.android.sleak.ui.base.BaseActivity
import com.aceinteract.android.sleak.ui.main.MainActivity
import com.aceinteract.android.sleak.util.StorageUtil

class SplashActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        scheduleSplashScreen()
    }

    private fun scheduleSplashScreen() {
        Handler().postDelayed({
            routeToActivity(StorageUtil(this@SplashActivity).currentUser)
            finish()
        }, 2000L)
    }

    private fun routeToActivity(user: Account?) = when (user) {
        null -> startActivity(Intent(this@SplashActivity, AuthActivity::class.java))
//        user.username.contains("@") -> startActivity(Intent(this@SplashActivity, ChooseUsernameActivity::class.java))
//        user.gender == 0 || user.date_of_birth == "" -> {
//            val nextIntent = Intent(this@SplashActivity, GenderDOBActivity::class.java)
//            nextIntent.putExtra("username", user.username)
//            startActivity(nextIntent)
//        }
//        user.user_following.size == 0 -> {
//            val nextIntent = Intent(this@SplashActivity, ChooseGenresActivity::class.java)
//            startActivity(nextIntent)
//        }
        else -> startActivity(Intent(this@SplashActivity, MainActivity::class.java))
    }
}
