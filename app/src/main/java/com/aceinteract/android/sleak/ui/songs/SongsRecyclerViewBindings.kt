package com.aceinteract.android.sleak.ui.songs

import android.databinding.BindingAdapter
import android.support.v7.widget.RecyclerView
import com.aceinteract.android.sleak.data.entity.Song

object SongsRecyclerViewBindings {

    @BindingAdapter("app:items")
    @JvmStatic fun setItems(recyclerView: RecyclerView, items: ArrayList<Song>) {
        with(recyclerView.adapter as SongsAdapter) {
            replaceData(items)
        }
    }

}