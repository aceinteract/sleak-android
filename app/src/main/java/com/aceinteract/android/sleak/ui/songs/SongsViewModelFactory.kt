package com.aceinteract.android.sleak.ui.songs

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.support.annotation.VisibleForTesting
import com.aceinteract.android.sleak.Injection
import com.aceinteract.android.sleak.SleakApplication
import com.aceinteract.android.sleak.data.repository.SongsRepository

class SongsViewModelFactory private constructor(
        private val application: SleakApplication,
        private val repository: SongsRepository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel> create(modelClass: Class<T>): T =
            with(modelClass) {
                when {
                    isAssignableFrom(SongsViewModel::class.java) -> SongsViewModel(application, repository)
                    else -> throw IllegalArgumentException("Unknown ViewModel class: ${modelClass.name}")
                }
            } as T

    companion object {

        @Volatile
        var INSTANCE: SongsViewModelFactory? = null

        fun getInstance(application: SleakApplication) =
                INSTANCE
                        ?: synchronized(SongsViewModelFactory::class.java) {
                            INSTANCE ?: SongsViewModelFactory(
                                    application,
                                    Injection.provideSongsRepository(application.applicationContext)
                            ).also { INSTANCE = it }
                        }

        @VisibleForTesting
        fun destroyInstance() {
            INSTANCE = null
        }

    }

}