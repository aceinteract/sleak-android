package com.aceinteract.android.sleak.ui.main.album


import android.arch.lifecycle.Observer
import android.graphics.Typeface
import android.os.Build
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import com.aceinteract.android.sleak.R
import com.aceinteract.android.sleak.SleakApplication
import com.aceinteract.android.sleak.data.model.Album
import com.aceinteract.android.sleak.data.model.Song
import com.aceinteract.android.sleak.databinding.FragmentAlbumBinding
import com.aceinteract.android.sleak.ui.base.BaseFragment
import com.aceinteract.android.sleak.util.setupSnackbar
import kotlin.system.measureTimeMillis

class AlbumFragment : BaseFragment() {

    private lateinit var albumId: String

    private lateinit var viewDataBinding: FragmentAlbumBinding

    private lateinit var songsAdapter: SongsAdapter

    val listener = object : ItemActionListener {
        override fun onArtistClicked(userId: String) {
            viewDataBinding.viewModel!!.openArtistEvent.value = userId
        }

        override fun onSongAddClicked(song: Song) {
            viewDataBinding.viewModel!!.addSongToLibraryEvent.value = song
        }

        override fun onSongClicked(song: Song) {
            viewDataBinding.viewModel!!.playSongEvent.value = song.trackNumber
        }

        override fun onAlbumAddClicked(album: Album) {
            viewDataBinding.viewModel!!.addToLibraryEvent.value = album.id
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        albumId = arguments!!.getString("albumId")
        viewDataBinding = FragmentAlbumBinding.inflate(inflater, container, false).apply {
            viewModel = obtainViewModel(AlbumViewModelFactory.getInstance(activity!!.application as SleakApplication))
            listener = this@AlbumFragment.listener
        }
        viewDataBinding.viewModel?.start(albumId)
        return viewDataBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewDataBinding.viewModel?.let {
            view?.setupSnackbar(this, it.snackbarMessage, Snackbar.LENGTH_LONG)
        }
        setupToolbar()
        setupAdapter()
        setupEventObservers()
    }

    override fun onStop() {
        super.onStop()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val window = activity!!.window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.statusBarColor = resources.getColor(R.color.color_primary_dark)
        }
    }

    private fun setupEventObservers() {

        viewDataBinding.viewModel!!.apply {
            playSongEvent.observe(this@AlbumFragment, Observer {
                it?.let { it1 -> playSong(it1) }
            })
            addSongToLibraryEvent.observe(this@AlbumFragment, Observer {
                addSongToLibrary(it!!)
            })
            addToLibraryEvent.observe(this@AlbumFragment, Observer {
                addAlbumToLibrary()
            })
            openArtistEvent.observe(this@AlbumFragment, Observer {
                it?.let { it1 -> openArtist(it1) }
            })
        }
    }

    private fun setupAdapter() {
        val viewModel = viewDataBinding.viewModel
        if (viewModel != null) {
            songsAdapter = SongsAdapter(ArrayList(0), viewModel)
            viewDataBinding.rvSongs.adapter = songsAdapter
            viewDataBinding.rvSongs.layoutManager = LinearLayoutManager(context)
        }
    }

    private fun setupToolbar() {
        context!!.setTheme(R.style.AppTheme_NoStatusBar)
        (activity!! as AppCompatActivity).setSupportActionBar(viewDataBinding.toolbar)
        (activity!! as AppCompatActivity).supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        (activity!! as AppCompatActivity).supportActionBar!!.setDisplayShowTitleEnabled(false)
        viewDataBinding.collapsingToolbar.isTitleEnabled = true
        val regularTypeface = Typeface.createFromAsset(context!!.assets, "fonts/Montserrat-Regular.ttf")
        viewDataBinding.collapsingToolbar.setCollapsedTitleTypeface(regularTypeface)
        viewDataBinding.collapsingToolbar.setExpandedTitleTypeface(regularTypeface)
        viewDataBinding.toolbar.setNavigationOnClickListener { closeFragment() }
    }

    private fun openArtist(userId: String) {

    }

    companion object {
        const val TAG = "AlbumFragment"
    }


}
