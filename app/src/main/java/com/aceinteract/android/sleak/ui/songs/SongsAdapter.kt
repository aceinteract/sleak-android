package com.aceinteract.android.sleak.ui.songs

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.aceinteract.android.sleak.data.entity.Song
import com.aceinteract.android.sleak.databinding.ItemSongBinding

class SongsAdapter(
        private var songs: ArrayList<Song>,
        private val songsViewModel: SongsViewModel
) : RecyclerView.Adapter<SongsAdapter.SongViewHolder>() {

    private lateinit var binding: ItemSongBinding

    fun replaceData(songs: ArrayList<Song>) {
        this.songs = songs
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SongViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        binding = ItemSongBinding.inflate(inflater, parent, false)

        return SongViewHolder(binding.root)
    }

    override fun getItemCount(): Int = songs.size

    override fun onBindViewHolder(holder: SongViewHolder, position: Int) {
//        val itemActionListener = object : SongItemActionListener {
//            override fun onSongClicked(song: Song) {
//                songsViewModel.playSongEvent.value = song.id
//            }
//        }
//
//        with(binding) {
//            song = songs[position]
//            playingSong = Song("", "", "", "")
//            listener = itemActionListener
//            executePendingBindings()
//        }
    }

    inner class SongViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

}