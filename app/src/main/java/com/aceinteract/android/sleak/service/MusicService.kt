package com.aceinteract.android.sleak.service

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.Intent
import android.media.AudioAttributes
import android.media.AudioManager
import android.media.MediaPlayer
import android.os.Binder
import android.os.Build
import android.os.IBinder
import android.os.PowerManager
import android.support.v4.app.NotificationCompat
import android.widget.Toast
import com.aceinteract.android.sleak.R
import com.aceinteract.android.sleak.SleakApplication
import com.aceinteract.android.sleak.data.model.Song
import com.aceinteract.android.sleak.ui.main.MainActivity
import com.aceinteract.android.sleak.util.StorageUtil

class MusicService : Service(), MediaPlayer.OnPreparedListener, MediaPlayer.OnErrorListener,
        MediaPlayer.OnCompletionListener, MediaPlayer.OnBufferingUpdateListener {

    private lateinit var storageUtil: StorageUtil

    private var musicPlayer = MediaPlayer()
    private var musicBinder = MusicBinder()

    private var mainIntent = Intent(SleakApplication.MAIN_BROADCAST)

    var currentSong = 0
    var songQueue = ArrayList<Song>()

    var bufferPercentage = 0

    var isPlaying: Boolean = false
        get() = musicPlayer.isPlaying

    var duration: Int = 0
        get() = musicPlayer.duration

    var seekPosition: Int = 0
        get() = musicPlayer.currentPosition

    private fun sendBroadcastToMain(isPlaying: Boolean) {
        if (songQueue.size > currentSong) {
            val song = songQueue[currentSong]
            mainIntent.putExtra(SleakApplication.BROADCAST_IS_PLAYING, isPlaying)
            mainIntent.putExtra(SleakApplication.BROADCAST_TITLE, song.title)
            mainIntent.putStringArrayListExtra(SleakApplication.BROADCAST_ARTISTS, song.artists.toStringArray())
            mainIntent.putExtra(SleakApplication.BROADCAST_SONG_ID, song.id)
            mainIntent.putExtra(SleakApplication.BROADCAST_ALBUM_TITLE, song.album.title)
            sendBroadcast(mainIntent)
        }
    }

    private fun showNotification() {
        val intent = Intent(this, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        val pendingIntent = PendingIntent.getActivity(this, OPEN_NOW_PLAYING_REQUEST,
                intent, PendingIntent.FLAG_UPDATE_CURRENT)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notificationChannel = NotificationChannel(CHANNEL_ID, getString(R.string.app_name),
                    NotificationManager.IMPORTANCE_LOW)
            getSystemService(NotificationManager::class.java).createNotificationChannel(notificationChannel)
        }

        val playAction = NotificationCompat.Action(R.drawable.ic_play, "Play", createPlayPauseAction(isPlaying))

        val builder = NotificationCompat.Builder(this, CHANNEL_ID)
        builder.setContentIntent(pendingIntent)
                .setSmallIcon(R.drawable.ic_sleak_transparent)
                .setTicker((songQueue[currentSong].title))
                .setStyle(android.support.v4.media.app.NotificationCompat.MediaStyle().setShowActionsInCompactView(0))
                .setOngoing(true)
                .setContentTitle(songQueue[currentSong].title)
                .setContentText(songQueue[currentSong].getStringArtists())
                .addAction(playAction)
        val notification = builder.build()
        startForeground(SleakApplication.MUSIC_NOTIFICATION_ID, notification)
    }

    private fun createPlayPauseAction(isPlaying: Boolean): PendingIntent {
        val intent = Intent(this, MusicService::class.java)
        if (isPlaying) intent.putExtra(PAUSE_SONG_EXTRA, true)
        else intent.putExtra(PLAY_SONG_EXTRA, true)
        return PendingIntent.getService(this, PLAY_PAUSE_REQUEST, intent, PendingIntent.FLAG_UPDATE_CURRENT)
    }

    private fun initializeMusicPlayer() {
        musicPlayer.setWakeMode(applicationContext, PowerManager.PARTIAL_WAKE_LOCK)
        musicPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC)
        musicPlayer.setOnPreparedListener(this)
        musicPlayer.setOnCompletionListener(this)
        musicPlayer.setOnErrorListener(this)
        musicPlayer.setOnBufferingUpdateListener(this)
    }

    fun startPlayer() {
        musicPlayer.start()

        sendBroadcastToMain(true)
    }

    fun playSong() {
        if (currentSong < songQueue.size) {
            musicPlayer.reset()

            val song = songQueue[currentSong]

            try {
                musicPlayer.setDataSource("${SleakApplication.baseUrl}api/v1/song/stream/${song.id}")
            } catch (e: Exception) {
                Toast.makeText(applicationContext, "There was a problem playing this song", Toast.LENGTH_LONG).show()
            }

            showNotification()

            musicPlayer.prepareAsync()
            sendBroadcastToMain(true)
        }
    }

    fun pauseSong() {
        musicPlayer.pause()

        sendBroadcastToMain(false)
    }

    fun seek(position: Int) = musicPlayer.seekTo(position)

    fun playPrev() {
        if (musicPlayer.currentPosition > 5000) {
            currentSong--
            if (currentSong < 0) currentSong = songQueue.size - 1
            playSong()
        } else playSong()
        storageUtil.currentQueuePosition = currentSong
    }

    fun playNext() {
        currentSong++
        if (currentSong > songQueue.size - 1) currentSong = 0
        playSong()
        storageUtil.currentQueuePosition = currentSong
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        if (intent != null) {
            if (intent.getBooleanExtra(PLAY_SONG_EXTRA, false)) playSong()
            if (intent.getBooleanExtra(PAUSE_SONG_EXTRA, false)) pauseSong()
        }
        return super.onStartCommand(intent, flags, startId)
    }

    override fun onCreate() {
        super.onCreate()
        initializeMusicPlayer()
        storageUtil = StorageUtil(applicationContext)
    }

    override fun onDestroy() {
        super.onDestroy()
        stopForeground(true)
    }

    override fun onBind(intent: Intent): IBinder = musicBinder

    override fun onUnbind(intent: Intent): Boolean {
        musicPlayer.stop()
        musicPlayer.release()
        return false
    }

    override fun onPrepared(mp: MediaPlayer) {
        mp.start()
    }

    override fun onError(mp: MediaPlayer, what: Int, extra: Int): Boolean {
        mp.reset()
        return false
    }

    override fun onCompletion(mp: MediaPlayer) {
        if (musicPlayer.currentPosition >= 0) {
            mp.reset()
            playNext()
        }
    }

    override fun onBufferingUpdate(mp: MediaPlayer, percent: Int) {
        bufferPercentage = percent
    }

    companion object {
        private const val TAG = "musicService"

        private const val CHANNEL_ID = "sleakNotification"

        private const val PLAY_SONG_EXTRA = "playSong"
        private const val PAUSE_SONG_EXTRA = "pauseSong"

        private const val OPEN_NOW_PLAYING_REQUEST = 0
        private const val PLAY_PAUSE_REQUEST = 1
    }

    inner class MusicBinder : Binder() {
        internal val service: MusicService
            get() = this@MusicService
    }

}