package com.aceinteract.android.sleak.ui.songs


import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.aceinteract.android.sleak.R
import com.aceinteract.android.sleak.databinding.FragmentSongsBinding
import com.aceinteract.android.sleak.ui.base.BaseFragment
import com.aceinteract.android.sleak.util.setupSnackbar

/**
 * A simple [Fragment] subclass.
 *
 */
class SongsFragment : BaseFragment() {

    private lateinit var viewDataBinding: FragmentSongsBinding
    private lateinit var recyclerAdapter: SongsAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        viewDataBinding = FragmentSongsBinding.inflate(inflater, container, false).apply {
            viewModel = (activity as SongsActivity).obtainViewModel()
        }
        return viewDataBinding.root
    }

    override fun onResume() {
        super.onResume()
        if (hasPermission(Manifest.permission.READ_EXTERNAL_STORAGE) &&
                hasPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)) viewDataBinding.viewModel?.start()
        else requestPermissions(arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE), REQUEST_EXTERNAL_STORAGE)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode == REQUEST_EXTERNAL_STORAGE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                viewDataBinding.viewModel?.start()
            } else {
                viewDataBinding.viewModel?.showSnackbarMessage(R.string.access_denied)
            }
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewDataBinding.viewModel?.let {
            view?.setupSnackbar(this, it.snackbarMessage, Snackbar.LENGTH_LONG)
        }
        setupAdapter()
        setupRefreshLayout()
    }

    private fun setupRefreshLayout() {
        viewDataBinding.swipeRefreshLayout.setColorSchemeColors(
                ContextCompat.getColor(activity!!, R.color.color_primary),
                ContextCompat.getColor(activity!!, R.color.color_accent),
                ContextCompat.getColor(activity!!, R.color.color_primary_dark)
        )
    }

    private fun setupAdapter() {
        val viewModel = viewDataBinding.viewModel
        if (viewModel != null) {
            recyclerAdapter = SongsAdapter(ArrayList(0), viewModel)
            viewDataBinding.songRecyclerView.adapter = recyclerAdapter
            viewDataBinding.songRecyclerView.layoutManager = LinearLayoutManager(context)
        } else {
            Log.w(TAG, "ViewModel not initialized when attempting to set up adapter.")
        }
    }

    companion object {
        fun newInstance() = SongsFragment()
        private const val TAG = "SongsFragment"
        const val REQUEST_EXTERNAL_STORAGE = 1234
    }

}
