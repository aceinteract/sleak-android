package com.aceinteract.android.sleak.ui.main.home


import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.aceinteract.android.sleak.R
import com.aceinteract.android.sleak.SleakApplication
import com.aceinteract.android.sleak.databinding.FragmentHomeBinding
import com.aceinteract.android.sleak.ui.base.BaseFragment
import com.aceinteract.android.sleak.ui.main.album.AlbumFragment
import com.aceinteract.android.sleak.util.setupSnackbar

class HomeFragment : BaseFragment() {

    private lateinit var viewDataBinding: FragmentHomeBinding
    private lateinit var recommendationsAdapter: RecommendationsAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        viewDataBinding = FragmentHomeBinding.inflate(inflater, container, false).apply {
            viewModel = obtainViewModel(HomeViewModelFactory.getInstance(activity!!.application as SleakApplication))
        }
        viewDataBinding.viewModel?.start()
        return viewDataBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewDataBinding.viewModel!!.openAlbumEvent.observe(this@HomeFragment, Observer {
            it?.let { it1 -> openAlbum(it1) }
        })

        viewDataBinding.viewModel?.let {
            view?.setupSnackbar(this, it.snackbarMessage, Snackbar.LENGTH_LONG)
        }
        setupAdapter()
        setupRefreshLayout()
    }

    private fun setupRefreshLayout() {
        viewDataBinding.swipeRefreshLayout.setColorSchemeColors(
                ContextCompat.getColor(activity!!, R.color.color_primary),
                ContextCompat.getColor(activity!!, R.color.color_accent),
                ContextCompat.getColor(activity!!, R.color.color_primary_dark)
        )
    }

    private fun setupAdapter() {
        val viewModel = viewDataBinding.viewModel
        if (viewModel != null) {
            recommendationsAdapter = RecommendationsAdapter(context!!, ArrayList(0), viewModel)
            viewDataBinding.rvRecommendations.adapter = recommendationsAdapter
            viewDataBinding.rvRecommendations.layoutManager = LinearLayoutManager(context)
        } else {
            Log.w(TAG, "ViewModel not initialized when attempting to set up adapter.")
        }
    }

    private fun openAlbum(albumId: String) {
        val albumFragment = AlbumFragment()
        val bundle = Bundle()
        bundle.putString("albumId", albumId)
        albumFragment.arguments = bundle
        openFragment(albumFragment, "$TAG|${AlbumFragment.TAG}")
    }

    companion object {
        const val TAG = "HomeFragment"
    }

}
