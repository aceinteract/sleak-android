package com.aceinteract.android.sleak.data.remote.source

import android.content.Context
import com.aceinteract.android.sleak.data.model.Recommendation
import com.aceinteract.android.sleak.data.remote.api.ApiCallback
import com.aceinteract.android.sleak.data.remote.api.ApiClient
import com.aceinteract.android.sleak.data.source.RecommendationsDataSource
import com.aceinteract.android.sleak.util.AppExecutors
import com.aceinteract.android.sleak.util.StorageUtil
import retrofit2.Call
import retrofit2.Response

class RecommendationsRemoteDataSource(val context: Context, val appExecutors: AppExecutors) : RecommendationsDataSource {

    private val apiClient = ApiClient()

    private val storageUtil = StorageUtil(context)

    override fun getRecommendations(callback: RecommendationsDataSource.LoadRecommendationsCallback) {
        appExecutors.networkIO.execute {

            apiClient.getHomeRecommendations(
                    storageUtil.currentUser!!.id,
                    object : ApiCallback<ArrayList<Recommendation>> {
                        override fun onFail(call: Call<ArrayList<Recommendation>>?, t: Throwable?) {

                            appExecutors.mainThread.execute { callback.onDataNotAvailable() }

                        }

                        override fun onResponse(call: Call<ArrayList<Recommendation>>?,
                                                response: Response<ArrayList<Recommendation>>?) {
                            appExecutors.mainThread.execute {

                                when(response!!.code()) {
                                    200 -> {
                                        callback.onRecommendationsLoaded(response.body()!!)
                                    }
                                    else -> callback.onDataNotAvailable()
                                }

                            }
                        }

                    })

        }
    }


}