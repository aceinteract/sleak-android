package com.aceinteract.android.sleak.data.source

import com.aceinteract.android.sleak.data.model.Album

interface AlbumsDataSource {

    interface GetAlbumCallback : DataSourceCallback {

        fun onAlbumLoaded(album: Album)

    }

    fun getAlbum(albumId: String, callback: GetAlbumCallback)

}